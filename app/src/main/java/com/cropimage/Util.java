package com.cropimage;
/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;

import com.bbm.sdk.support.util.Logger;


/**
 * Collection of utility functions used in this package.
 */
public class Util {

    private Util() {
    }


    // Rotates the bitmap by the specified degree. Creates a new bitmap and recycles the src.
    public static Bitmap rotateImage(final Bitmap src, final Point size, final float degree) {
        // create new matrix
        final Matrix matrix = new Matrix();
        // setup rotation degree
        matrix.postRotate(degree);
        try {
            final float scaleWidth = ((float) size.x) / src.getWidth();
            final float scaleHeight = ((float) size.y) / src.getHeight();
            float scale;
            if (scaleHeight > scaleWidth) {
                scale = scaleHeight;
            } else {
                scale = scaleWidth;
            }
            // only scale if we're decreasing the image
            // we don't want to scale up
            if(scale < 1.0f) {
                matrix.preScale(scale, scale);
            }
            final Bitmap bmp = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
            src.recycle();
            return bmp;
        } catch (final OutOfMemoryError e) {
            // Can be possible for large bitmaps to cause out of memory exceptions. return original
            Logger.e(e);
            return src;
        }
    }
}
