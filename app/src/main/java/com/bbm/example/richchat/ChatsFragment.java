/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bbm.example.common.ui.AppUserSelectedCallback;
import com.bbm.example.common.ui.ChatWithUserPrompter;
import com.bbm.example.common.ui.ChooseAppUserDialog;
import com.bbm.example.common.util.PermissionsUtil;
import com.bbm.example.richchat.media.InCallActivity;
import com.bbm.example.richchat.media.MediaUtils;
import com.bbm.example.richchat.util.RichChatUtils;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.bbmds.Chat;
import com.bbm.sdk.bbmds.internal.lists.ObservableList;
import com.bbm.sdk.media.BBMECall;
import com.bbm.sdk.media.BBMEMediaManager;
import com.bbm.sdk.reactive.ObservableMonitor;
import com.bbm.sdk.reactive.Observer;
import com.bbm.sdk.service.BBMEnterpriseState;
import com.bbm.sdk.support.identity.user.AppUser;
import com.bbm.sdk.support.reactive.AbstractObservableList;
import com.bbm.sdk.support.reactive.ObserveConnector;
import com.bbm.sdk.support.util.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ChatsFragment extends Fragment {
    private ObserveConnector mObserveConnector = new ObserveConnector();

    private final ObservableList<Chat> mChats = BBMEnterprise.getInstance().getBbmdsProtocol().getChatList();

    private final ObservableList<ChatListItem> mChatListItems = new AbstractObservableList<ChatListItem>() {
        private List<ChatListItem> mList;

        @Override
        protected void init() {
            super.init();

            mObserveConnector.connect(mChats, new Observer() {
                @Override
                public void changed() {
                    if (mChats.isPending() && mEmptyView != null) {
                        mEmptyView.setVisibility(View.VISIBLE);
                        mList = new ArrayList<>();
                        return;
                    }

                    List<ChatListItem> tmp = new ArrayList<>(mChats.size());
                    for (int i=0;i<mChats.size();++i) {
                        Chat chat = mChats.get(i);
                        if (chat.state != Chat.State.Defunct && !chat.hasFlag(Chat.Flags.Hidden)) {
                            tmp.add(new ChatListItem(getContext(), chat));
                        }
                    }

                    //Sort the list by timestamp
                    Collections.sort(tmp, new Comparator<ChatListItem>() {
                        @Override
                        public int compare(ChatListItem o1, ChatListItem o2) {
                            long diff = o2.getTimestamp() - o1.getTimestamp();
                            if (diff == 0) {
                                return 0;
                            } else if (diff > 0) {
                                return 1;
                            } else {
                                return -1;
                            }
                        }
                    });

                    mList = tmp;

                    notifyObservers();

                    if (mEmptyView != null) {
                        mEmptyView.setVisibility(View.GONE);
                    }
                }
            }, true);
        }

        @Override
        public int size() {
            return mList.size();
        }

        @Override
        public ChatListItem get(int index) {
            return mList.get(index);
        }

        @Override
        public boolean isPending() {
            return mChats.isPending();
        }

        @Override
        public List<ChatListItem> get() {
            return mList;
        }
    };

    /**
     * A monitor to track if an active call is in progress.
     * If a call is occurring change the icon of the button.
     */
    private ObservableMonitor mVoiceCallMonitor = new ObservableMonitor() {
        @Override
        protected void run() {
            try {
                if (BBMEnterprise.getInstance().getState().get() == BBMEnterpriseState.STARTED) {
                    BBMEMediaManager mediaManager = BBMEnterprise.getInstance().getMediaManager();
                    int activeCallId = mediaManager.getActiveCallId().get();
                    if (mediaManager.getCall(activeCallId).get().getCallState() != BBMECall.CallState.CALL_STATE_IDLE) {
                        mStartCallFloatingButton.setImageResource(R.drawable.ic_voice_active_call);
                        return;
                    }
                }

                mStartCallFloatingButton.setImageResource(R.drawable.ic_voice_call);
            } catch (IllegalStateException ise) {
                Logger.e(ise, "Failed to get mediaManager");
            }
        }
    };

    private ListView mList;
    private View mEmptyView;

    private ChatsFragmentListAdapter mAdapter;
    private FloatingActionButton mStartCallFloatingButton;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        final View v = inflater.inflate(R.layout.fragment_chats, container, false);

        mEmptyView = v.findViewById(R.id.chats_empty_layout);
        mList = (ListView) v.findViewById(R.id.chatslist);

        mAdapter = new ChatsFragmentListAdapter(mChatListItems, getActivity());

        mList.setAdapter(mAdapter);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChatListItem item = mChatListItems.get(position);
                Intent intent = new Intent(getActivity(), RichChatActivity.class);
                intent.putExtra(RichChatActivity.EXTRA_CHAT_ID,  item.getChat().chatId);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_and_scale_in, R.anim.fade_out);
            }
        });

        View startChatFloatingButton = v.findViewById(R.id.start_chat_fab);
        startChatFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //no title, just text in the button to save space
                ChooseAppUserDialog.promptToSelectMultiple(getActivity(), null, getString(R.string.start_chat), getString(R.string.chat_subject), new AppUserSelectedCallback() {
                    @Override
                    public void selected(Collection<AppUser> contacts, String extraText) {
                        Logger.d("onClick: selected contacts="+contacts);
                        if (contacts != null && contacts.size() > 0) {
                            long[] regIds = new long[contacts.size()];
                            int i = 0;
                            for (AppUser user : contacts) {
                                regIds[i] = user.getRegId();
                                ++i;
                            }
                            RichChatUtils.createNewChat(getActivity(), regIds, extraText);
                        }
                    }

                    @Override
                    public void selected(AppUser contact) {
                        //this one won't be called in multiselect mode
                    }
                }, null); //no filter needed here to preselecte any contacts
            }
        });

        mStartCallFloatingButton = (FloatingActionButton)v.findViewById(R.id.start_call_fab);
        mStartCallFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BBMEMediaManager mediaManager = BBMEnterprise.getInstance().getMediaManager();
                int activeCallId = mediaManager.getActiveCallId().get();
                if (mediaManager.getCall(activeCallId).get().getCallState() != BBMECall.CallState.CALL_STATE_IDLE) {
                    //We are in a call, reopen the call activity
                    Intent inCallIntent = new Intent(getActivity(), InCallActivity.class);
                    inCallIntent.putExtra(InCallActivity.EXTRA_CALL_ID, activeCallId);
                    getActivity().startActivity(inCallIntent);
                } else {
                    RichChatUtils.promptToStartCall(getActivity());
                }
            }
        });

        return v;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.chats_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Logger.d("onOptionsItemSelected: "+item);

        switch (item.getItemId()) {
            case R.id.start_chat_by_regid:
                //This is an uglier way than the contact picker dialog, but for testing allows entering regid manually
                //can be used to communicate with other apps with different user management...
                ChatWithUserPrompter.promptToCreateNewChat(getActivity(), new ChatWithUserPrompter.RegidSelectedCallback() {
                    @Override
                    public void selected(long[] regIds, String chatSubject) {
                        RichChatUtils.createNewChat(getContext(), regIds, chatSubject);
                    }
                });
                return true;
            case R.id.start_call_by_regid:
                BBMEMediaManager mediaManager = BBMEnterprise.getInstance().getMediaManager();
                int activeCallId = mediaManager.getActiveCallId().get();
                if (mediaManager.getCall(activeCallId).get().getCallState() != BBMECall.CallState.CALL_STATE_IDLE) {
                    //We are in a call, reopen the call activity
                    Intent inCallIntent = new Intent(getActivity(), InCallActivity.class);
                    inCallIntent.putExtra(InCallActivity.EXTRA_CALL_ID, activeCallId);
                    getActivity().startActivity(inCallIntent);
                } else {
                    CallUserPrompter.promptToStartCall(getActivity(), new CallUserPrompter.SelectedRegIdCallback() {
                        @Override
                        public void selectedRegId(long regId) {
                            //Start a call (including permission check)
                            MediaUtils.makeCall(getActivity(), ChatsFragment.this, regId);
                        }
                    });
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
        mVoiceCallMonitor.activate();
    }

    @Override
    public void onPause() {
        super.onPause();
        mVoiceCallMonitor.dispose();
    }

    @Override
    public void onDestroy() {
        mList.removeAllViewsInLayout();
        mList.clearFocus();

        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Logger.d("onRequestPermissionsResult: requestCode=" + requestCode + " " + PermissionsUtil.resultsToString(permissions, grantResults));

        //neither permissions or grantResults should be empty but google docs warns they could be and should be treated as a cancellation
        if (permissions.length == 0 && grantResults.length == 0) {
            Logger.w("empty permissions and/or grantResults");
            return;
        }

        if (requestCode == PermissionsUtil.PERMISSION_RECORD_AUDIO_FOR_VOICE_CALL) {
            if (PermissionsUtil.isGranted(grantResults, 0)) {
                //If the user granted us permission to start the call we can do so immediately.
                MediaUtils.makeCallPermissionGranted(getActivity(), this);
            } else {
                PermissionsUtil.displayCanNotContinueIfCanNotAsk(getActivity(), android.Manifest.permission.RECORD_AUDIO,
                        R.string.rationale_record_audio_denied);
            }
        }
    }

}
