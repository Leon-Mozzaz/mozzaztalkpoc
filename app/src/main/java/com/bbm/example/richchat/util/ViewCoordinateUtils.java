/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat.util;


import android.graphics.Rect;
import android.view.View;

public class ViewCoordinateUtils {

    /**
     * Utility method to check if x,y coordinates belong to the given view
     *
     * @param x
     * @param y
     * @param view
     * @return boolean
     */
    public static boolean coordinateBelongsToView(final int x, final int y, final View view) {
        final int[] viewCoordinate = new int[2];
        view.getLocationOnScreen(viewCoordinate);

        final int viewWidth = view.getWidth();
        final int viewHeight = view.getHeight();

        final Rect r = new Rect(viewCoordinate[0], viewCoordinate[1], viewCoordinate[0] + viewWidth, viewCoordinate[1]
                + viewHeight);

        return r.contains(x, y);
    }
}
