/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

public class SendEditText extends AppCompatEditText {

    private boolean mKeyboardEnterAsNewLineAllowed = false;

    public SendEditText(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    public SendEditText(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public SendEditText(final Context context) {
        super(context);
    }

    @Override
    public InputConnection onCreateInputConnection(@NonNull final EditorInfo outAttrs) {

        boolean enterAsNewLine = mKeyboardEnterAsNewLineAllowed ;//&& Alaska.getSettings().isKeyboardEnterAsNewLine();
        if(enterAsNewLine) {
            outAttrs.imeOptions |= EditorInfo.IME_ACTION_NONE;
        } else {
            outAttrs.imeOptions |= EditorInfo.IME_ACTION_SEND;
        }
        final InputConnection connection = super.onCreateInputConnection(outAttrs);
        if(!enterAsNewLine) {
            /* This manual after-the-fact override appears to be the only way to
             * get a multiline editbox with a Send key instead of an Enter key. */
            outAttrs.imeOptions &= ~EditorInfo.IME_FLAG_NO_ENTER_ACTION;
        }
        // landscape support, so the keyboard doesn't cover the whole screen
        outAttrs.imeOptions |= EditorInfo.IME_FLAG_NO_FULLSCREEN;
        return connection;
    }

    /**
     * Determine if keyboard enters new line setting is applicable to this
     * EditText
     * @param keyboardEnterAsNewLineAllowed true if it is, false otherwise
     */
    public void setKeyboardEnterAsNewLineOverrideAllowed(boolean keyboardEnterAsNewLineAllowed) {
        this.mKeyboardEnterAsNewLineAllowed = keyboardEnterAsNewLineAllowed;
    }
}
