/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat.media;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.media.BBMEIncomingCallObserver;

/**
 * The IncomingCallObserver will be notified when a new call has arrived.
 */

public class IncomingCallObserver implements BBMEIncomingCallObserver{

    public Context mContext;

    public IncomingCallObserver(Context context) {
        mContext = context;
    }

    @Override
    public void onIncomingCall(final int callId) {
        //If we have audio permissions we can accept the call immediately
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            BBMEnterprise.getInstance().getMediaManager().acceptCall(callId);
        }

        Intent incomingCallIntent = new Intent(mContext, IncomingCallActivity.class);
        incomingCallIntent.putExtra(IncomingCallActivity.INCOMING_CALL_ID, callId);
        incomingCallIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(incomingCallIntent);
    }
}
