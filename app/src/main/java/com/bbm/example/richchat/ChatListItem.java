/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.content.Context;

import com.bbm.sdk.support.util.BbmUtils;
import com.bbm.sdk.bbmds.Chat;


public class ChatListItem {
    private Context mContext;

    private Chat mChat;

    /**
     * @param chat the chat
     */
    public ChatListItem(Context context, final Chat chat) {
        super();
        mContext = context;
        this.mChat = chat;
    }


    public String getPrimaryKey() {
        return mChat.getPrimaryKey();
    }


    /**
     * @return the mChat
     */
    public Chat getChat() {
        return mChat;
    }

    public long getTimestamp() {
        return mChat.lastMessage;
    }

    /**
     *
     * @return Uri of this chatListItem
     */
    public String getUri() {
        return BbmUtils.chatIdToConversationUri(mChat.chatId);
    }

    /**
     *
     * @return if the Conversation/Hosted Chat is a conference
     */
    public boolean isConference() {
        return !mChat.isOneToOne;
    }
}
