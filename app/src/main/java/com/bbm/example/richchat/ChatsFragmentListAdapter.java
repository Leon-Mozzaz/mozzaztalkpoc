/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bbm.sdk.support.reactive.ObserveConnector;
import com.bbm.sdk.reactive.ObservableValue;
import com.bbm.sdk.reactive.Observer;

import java.util.List;

public class ChatsFragmentListAdapter extends BaseAdapter {
    private Activity mActivity;
    private ObserveConnector mObserveConnector = new ObserveConnector();

    private final ObservableValue<List<ChatListItem>> mList;

    public ChatsFragmentListAdapter(final ObservableValue<List<ChatListItem>> list, final Activity activity) {
        mList = list;
        mActivity = activity;

        mObserveConnector.connect(mList, new Observer() {
            @Override
            public void changed() {
                notifyDataSetChanged();
            }
        });
    }

    protected String getKeyFor(final ChatListItem item) {
        return item.getPrimaryKey();
    }

    @Override
    public int getCount() {
        return mList.get().size();
    }

    @Override
    public ChatListItem getItem(int position) {
        return mList.get().get(position);
    }

    @Override
    public long getItemId(int position) {
        return mList.get().get(position).getPrimaryKey().hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ChatListItem item = getItem(position);
        if (convertView != null) {

            final ChatListItemViewHolder vh = (ChatListItemViewHolder) convertView.getTag();
            vh.updateView(item);

            return convertView;
        } else {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.list_item_chat, parent, false);
            final ChatListItemViewHolder vh = new ChatListItemViewHolder(mActivity, view);
            view.setTag(vh);
            vh.updateView(item);
            return view;
        }
    }
}
