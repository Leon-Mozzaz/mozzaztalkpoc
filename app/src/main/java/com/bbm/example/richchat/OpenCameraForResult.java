/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;

import com.bbm.sdk.support.util.Logger;
import com.bbm.example.richchat.image.ImageUtil;
import com.bbm.example.richchat.util.RichChatUtils;

/**
 * Helper to launch the camera and create an image if necessary. The method will
 * verify the given activity has the required permissions to use the camera, callers
 * will need to ensure the activity handles the permission call back for
 * PERMISSION_CAMERA_REQUEST and PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_CAMERA.
 *
 * In some cased the time taken to process the camera folder can be long, so running in
 * a runnable will help avoid ANR situations. Also, as the callback will check for
 * permissions, this runnable MUST BE posted on the UI Thread, hence the tag @UiThread.
 * Callers must implement the Callback to obtain the Uri to the image directory.
 *
 * @return The URI to the image location or null.
 */

@UiThread
public final class OpenCameraForResult implements Runnable {

    private final Activity mActivity;
    private final int mRequestCode;
    private volatile boolean mCancelled = false;

    public OpenCameraForResult(@NonNull final Activity activity, final int requestCode) {

        if (!(activity instanceof Callback)) {
            throw new IllegalArgumentException("Activity must implement the OnCameraOpenTaskComplete callback");
        }

        mActivity = activity;
        mRequestCode = requestCode;
    }

    @Override
    public void run() {
        // Ask fo the permissions first, because we need to show a UI, this needs
        // to be done on the ui thread. So if this returns false, a dialog will be
        // shown to the user, but we need to now complete this task also. So
        // exit early and call the OnCameraOpenTaskComplete.
        if (!RichChatUtils.checkOrPromptCameraAndWrite(mActivity)) {
            ((Callback) mActivity).OnCameraOpenTaskComplete(null);
            return;
        }

        // At this point a async task can be run to finish the operation. This will
        // be in the back ground.
        AsyncTask<Void, Void, Uri> task = new AsyncTask<Void, Void, Uri>() {
            @Override
            protected Uri doInBackground(Void... voids) {
                return ImageUtil.getOutputMediaFileUri(ImageUtil.MEDIA_TYPE_IMAGE, ImageUtil.MIME_TYPE_JPEG);
            }

            @Override
            protected void onPostExecute(Uri result) {
                // If the activity is destroyed, finishing or operation cancelled then no-op the
                // method.
                if (mActivity.isDestroyed() || mActivity.isFinishing() || mCancelled) {
                    return;
                }

                // Call the callback first.
                ((Callback) mActivity).OnCameraOpenTaskComplete(result);

                // Start the activity with the URI
                try {
                    final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    ContentValues values = new ContentValues();
                    values.put(MediaStore.Images.Media.DATA, result.getPath());

                    //Get a content:// uri
                    Uri uri = mActivity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

                    mActivity.startActivityForResult(intent, mRequestCode);
                } catch (final ActivityNotFoundException e) {
                    //Optionally, in the future we should throw a toast
                    //Perhaps this device doesn't have a camera app, or is a tablet with no camera
                    //or doesn't have an sd card?
                    Logger.e(e);
                }
            }
        };
        task.execute();
    }


    public void cancel(@NonNull final Handler handler) {
        mCancelled = true;
        handler.removeCallbacks(this);
        ((Callback) mActivity).OnCameraOpenTaskComplete(null);
    }

    /**
     * Interface to handle the callback of this operation.
     */
    public interface Callback {

        /**
         * The callback to the open camera operation.
         * @param uri the path to the image directory.
         */
        void OnCameraOpenTaskComplete(final Uri uri);
    }
}
