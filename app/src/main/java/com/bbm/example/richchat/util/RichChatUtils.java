/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bbm.example.common.ui.AppUserSelectedCallback;
import com.bbm.example.common.ui.ChooseAppUserDialog;
import com.bbm.example.common.util.PermissionsUtil;
import com.bbm.example.common.util.Utils;
import com.bbm.example.richchat.R;
import com.bbm.example.richchat.RichChatActivity;
import com.bbm.example.richchat.media.MediaUtils;
import com.bbm.sdk.bbmds.inbound.ChatStartFailed;
import com.bbm.sdk.bbmds.internal.Existence;
import com.bbm.sdk.bbmds.internal.lists.ObservableList;
import com.bbm.sdk.support.identity.user.AppUser;
import com.bbm.sdk.support.util.ChatStartHelper;
import com.bbm.sdk.support.util.Logger;

import java.lang.ref.WeakReference;
import java.util.Collection;

public class RichChatUtils {

    private static WeakReference<Toast> mToast;

    private static Handler sMainHandler = new Handler(Looper.getMainLooper());

    /**
     * Create and show a default toast
     *
     * @param text    : text of the Toast
     */
    public static void showDefaultToast(final Context context, final String text) {
        showDefaultToast(context, text, null, -1, -1, -1, Toast.LENGTH_LONG, true);
    }

    /**
     * Create and show a default toast
     *
     * @param text    : text of the Toast
     */
    public static void showDefaultToast(final Context context, final String text, final int duration) {
        showDefaultToast(context, text, null, -1, -1, -1, duration, true);
    }

    /**
     * Create and show a custom toast
     *
     * @param text     : text of the Toast
     * @param title    : title of the Toast
     * @param gravity  : gravity of the Toast
     * @param offsetX  : X offset of the Toast
     * @param offsetY  : Y offset of the Toast
     * @param duration : duration of the Toast
     */
    public static void showDefaultToast(final Context context, final String text, final String title, final int gravity, final int offsetX, final int offsetY, final int duration, final boolean isRefs) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            //Already UI thread, safe to call directly
            showDefaultToastOnUI(context, text, title, gravity, offsetX, offsetY, duration, isRefs);
        } else {
            //can't do this from background thread, need to post to UI thread
            sMainHandler.post(new Runnable() {
                @Override
                public void run() {
                    showDefaultToastOnUI(context, text, title, gravity, offsetX, offsetY, duration, isRefs);
                }
            });
        }
    }

    private static void showDefaultToastOnUI(final Context context, final String text, final String title, final int gravity, final int offsetX, final int offsetY, final int duration, final boolean isRefs) {
        dismissSnackBarAndToast();
        Toast toast = new Toast(context.getApplicationContext());
        final LayoutInflater inflater = LayoutInflater.from(context.getApplicationContext());
        final View toastView = inflater.inflate(R.layout.view_custom_toast, null, false);
        if (title != null) {
            final TextView titleLabel = (TextView) toastView.findViewById(R.id.view_custom_toast_title);
            titleLabel.setText(title);
            titleLabel.setVisibility(View.VISIBLE);
        }
        final TextView toastLabel = (TextView) toastView.findViewById(R.id.view_custom_toast_label);
        toastLabel.setText(text);
        toast.setDuration(duration);
        if (offsetX > 0 || offsetY > 0 || gravity > 0) {
            toast.setGravity(gravity, offsetX, offsetY);
        }
        toast.setView(toastView);
        toast.show();
        if (isRefs) {
            mToast = new WeakReference<>(toast);
        }
    }



    /**
     * Helper to dismiss existing SnackBar and Toast
     */
    public static void dismissSnackBarAndToast() {

        if (mToast != null && mToast.get() != null) {
            mToast.get().cancel();
            mToast.clear();
        }
    }

    public static void createNewChat(final Context context, Collection<AppUser> appUsers, String chatSubject) {
        long[] regIds = new long[appUsers.size()];
        int i = 0;
        for (AppUser appUser : appUsers) {
            regIds[i] = appUser.getRegId();
            ++i;
        }
        createNewChat(context, regIds, chatSubject);
    }

    public static void createNewChat(final Context context, final long[] regIds, String chatSubject) {
        ChatStartHelper.startNewChat(regIds, chatSubject, new ChatStartHelper.ChatStartedCallback() {
            @Override
            public void onChatStarted(String chatId) {
                Intent intent = new Intent(context, RichChatActivity.class);
                intent.putExtra(RichChatActivity.EXTRA_CHAT_ID, chatId);
                context.startActivity(intent);
            }

            @Override
            public void onChatStartFailed(ChatStartFailed.Reason reason) {
                Logger.user("Failed to create chat due to "+reason+" with regIds="+regIds);
            }
        });
    }


    public static void promptToStartCall(final Activity activity) {
        promptToStartCall(activity, null);
    }

    public static void promptToStartCall(final Activity activity, ObservableList<AppUser> usersToPickFrom) {
        //set title, null for button text to not show one since the user just has to tap user
        //also null for edit text hint since it isn't needed
        ChooseAppUserDialog.promptToSelect(activity, activity.getString(R.string.start_call), null, null, new AppUserSelectedCallback() {
            @Override
            public void selected(Collection<AppUser> appUsers, String extraText) {
                //this one won't be called in non-multiselect mode
            }

            @Override
            public void selected(AppUser appUser) {
                Logger.d("onClick: selected appUser="+appUser);
                if (appUser.getExists() == Existence.YES) {
                    MediaUtils.makeCall(activity, appUser.getRegId());
                }
            }
        }, usersToPickFrom);
    }

    private static String toString(long[] ids) {
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (int i=0;i<ids.length;++i) {
            sb.append(ids[i]);
            if (i < ids.length - 1) {
                sb.append(", ");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    /**
     * Helper to ask for both permissions need for camera and write. In future it would be nice
     * to have 1 dialog to display reasons for 1 or more permissions.
     *
     * NOTE: Users of this method will need to PERMISSION_CAMERA_REQUEST and
     * PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_CAMERA permission requests.
     */
    public static boolean checkOrPromptCameraAndWrite(final Activity activity) {

        if(Utils.isMarshmallowOrHigher()) {
            if (!PermissionsUtil.checkOrPromptSelfPermission(activity, Manifest.permission.CAMERA,
                    PermissionsUtil.PERMISSION_CAMERA_REQUEST,
                    R.string.rationale_camera)) {
                return false;
            }

            if (!PermissionsUtil.checkOrPromptSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    PermissionsUtil.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_CAMERA,
                    R.string.rationale_write_external_storage)) {
                return false;
            }
        }

        return true;
    }

}
