/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat.fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bbm.example.common.R;
import com.bbm.example.common.ui.ImageTask;
import com.bbm.example.common.util.PermissionsUtil;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.bbmds.BbmdsProtocol;
import com.bbm.sdk.bbmds.GlobalAuthTokenState;
import com.bbm.sdk.bbmds.GlobalLocalPin;
import com.bbm.sdk.bbmds.GlobalSetupState;
import com.bbm.sdk.bbmds.User;
import com.bbm.sdk.bbmds.internal.Existence;
import com.bbm.sdk.bbmds.outbound.AuthToken;
import com.bbm.sdk.reactive.ObservableValue;
import com.bbm.sdk.reactive.Observer;
import com.bbm.sdk.service.BBMEnterpriseState;
import com.bbm.sdk.service.ProtocolMessage;
import com.bbm.sdk.service.ProtocolMessageConsumer;
import com.bbm.sdk.support.identity.auth.AuthenticatedAccountData;
import com.bbm.sdk.support.identity.auth.google.auth.GoogleAccessTokenUpdater;
import com.bbm.sdk.support.identity.auth.google.auth.GoogleAuthHelper;
import com.bbm.sdk.support.identity.auth.google.auth.GoogleGetAccessTokenTask;
import com.bbm.sdk.support.identity.user.AppUser;
import com.bbm.sdk.support.identity.user.UserManager;
import com.bbm.sdk.support.identity.user.firebase.FirebaseUserDbSync;
import com.bbm.sdk.support.util.BbmUtils;
import com.bbm.sdk.support.util.Logger;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;


/**
 * The main Sign-in Fragment that will use a Google Account on the device to attempt obtain a Google access token to
 * be sent into BBM SDK to authorize the service.
 */
public class GoogleConnectSetupFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, GoogleGetAccessTokenTask.OnGetAccessTokenResult,
        GoogleAuthHelper.SilentSignResultCallback {

    private BBMEnterprise mBbmEnterprise;

    //Global values to monitor during setup
    private ObservableValue<GlobalSetupState> mSetupState;
    private ObservableValue<GlobalAuthTokenState> mAuthTokenState;
    private ObservableValue<GlobalLocalPin> mLocalPin;
    private ObservableValue<User> mLocalBbmUser;
    private ObservableValue<BBMEnterpriseState> mBbmEnterpriseState;  //overall state of the service
    private ObservableValue<AppUser> mLocalAppUser;


    private static final int RC_SIGN_IN = 9001; // The result code to be used when calling startActivityForResult

    //UI elements
    private TextView mEnterpriseStateView;
    private TextView mSetupStateView;
    private TextView mAuthTokenStateView;
    private TextView mLocalPinView;
    private TextView mLocalRegIdView;
    private TextView mDomainView;
    private TextView mGoogleEmailView;

    private TextView mGoogleNameView;

    private ImageView mGoogleAvatarView;

    private TextView mSetupErrorView;
    private TextView mServiceFailureView;
    private Button mServiceStopButton;
    private Button mSendLogFilesButton;

    private View mSetupErrorContainer; //for controlling visibility of error msg only
    private View mServiceFailureContainer; //for controlling visibility of the service failure msg
    private Button mRetryButton;
    private Button mRestartButton;

    private GoogleApiClient mGoogleApiClient;
    private SignInButton mSignInButton;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View contentView = inflater.inflate(R.layout.fragment_google_setup, container, false);

        // Observe states and values for the BBMEnterprise SDK
        // get the service
        mBbmEnterprise = BBMEnterprise.getInstance();

        mBbmEnterpriseState = mBbmEnterprise.getState();
        mBbmEnterpriseState.addObserver(mEnterpriseStateObserver);

        // add message consumer to monitor for incoming setupError messages
        mBbmEnterprise.getBbmdsProtocolConnector().addMessageConsumer(mSetupErrorListener);

        final BbmdsProtocol protocol = mBbmEnterprise.getBbmdsProtocol();
        //fetch setup related globals for monitoring
        mSetupState = protocol.getGlobalSetupState();
        mSetupState.addObserver(mSetupStateObserver);

        mAuthTokenState = protocol.getGlobalAuthTokenState();
        mAuthTokenState.addObserver(mAuthTokenStateObserver);

        mLocalPin = protocol.getGlobalLocalPin();
        mLocalPin.addObserver(mLocalPinObserver);

        mLocalBbmUser = BbmUtils.getLocalBbmUser();
        mLocalBbmUser.addObserver(mLocalBbmUserObserver);

        mLocalAppUser = UserManager.getInstance().getLocalAppUser();
        mLocalAppUser.addObserver(mLocalAppUserObserver);

        // Load the view.
        mEnterpriseStateView = (TextView) contentView.findViewById(R.id.service_state);

        mSetupStateView = (TextView) contentView.findViewById(R.id.setup_state);
        mServiceFailureContainer = contentView.findViewById(R.id.service_failure);
        mServiceFailureView = (TextView) contentView.findViewById(R.id.service_failure_reason);
        mAuthTokenStateView = (TextView) contentView.findViewById(R.id.auth_token_state);
        mLocalPinView = (TextView) contentView.findViewById(R.id.local_pin);
        mLocalRegIdView = (TextView) contentView.findViewById(R.id.local_regid);

        mDomainView = (TextView) contentView.findViewById(R.id.domain);
        mGoogleEmailView = (TextView) contentView.findViewById(R.id.google_email);
        mGoogleNameView = (TextView) contentView.findViewById(R.id.google_name);
        mGoogleAvatarView = (ImageView) contentView.findViewById(R.id.contact_avatar);

        mSetupErrorContainer = contentView.findViewById(R.id.setup_error);
        mSetupErrorView = (TextView) contentView.findViewById(R.id.error_message);

        mRetryButton = (Button) contentView.findViewById(R.id.signin_retry_button);
        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRetryButton.setVisibility(View.GONE);
                mSetupErrorContainer.setVisibility(View.GONE);
                mSetupErrorView.setText("");

                GoogleAuthHelper.handleSilentSignin(mGoogleApiClient, GoogleConnectSetupFragment.this);
            }
        });

        mRestartButton = (Button) contentView.findViewById(R.id.service_restart_button);
        mRestartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRestartButton.setVisibility(View.GONE);
                //Restart BBM
                startBBMEnterpriseService();
            }
        });

        //STOP button
        mServiceStopButton = (Button) contentView.findViewById(R.id.service_stop_button);
        mServiceStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mServiceStopButton.isEnabled()) {
                    mServiceStopButton.setEnabled(false);  //disable button to avoid multiple clicks
                    mRetryButton.setVisibility(View.GONE);
                    mSetupErrorContainer.setVisibility(View.GONE);
                    mSetupErrorView.setText("");

                    try {
                        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                                new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(@NonNull Status status) {
                                        stopBBMEnterpriseService();
                                    }
                                });
                    } catch (IllegalStateException ise) {
                        Logger.e(ise, "Failed to sign out of GoogleSign");
                        mSetupErrorContainer.setVisibility(View.VISIBLE);
                        mSetupErrorView.setText("Google Sign Out: "+ise);

                        stopBBMEnterpriseService();
                    }

                }
            }
        });


        //Send Log files button
        mSendLogFilesButton = (Button) contentView.findViewById(R.id.send_log_files);
        mSendLogFilesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PermissionsUtil.checkOrPromptSelfPermission(getActivity(), GoogleConnectSetupFragment.this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        PermissionsUtil.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_FILES, R.string.rationale_write_external_storage, null)) {
                    BbmUtils.sendBbmLogFiles(getContext());
                }
            }
        });

        mGoogleApiClient = GoogleAuthHelper.getApiClient(this.getContext(), GoogleAccessTokenUpdater.getInstance().getClientServerId());

        mSignInButton = (SignInButton) contentView.findViewById(R.id.google_sign_in_button);
        mSignInButton.setSize(SignInButton.SIZE_STANDARD);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        // initialize the view
        updateServiceButtonState(true);

        try {
            Bundle bundle = getContext().getPackageManager().getApplicationInfo(getContext().getPackageName(), PackageManager.GET_META_DATA).metaData;
            if (bundle != null) {
                mDomainView.setText(bundle.getString("com.bbm.sdk.UserDomain"));
            }
        } catch (PackageManager.NameNotFoundException nfe) {
            Logger.w(nfe);
        }

        return contentView;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Logger.d("onRequestPermissionsResult: requestCode=" + requestCode + " " + PermissionsUtil.resultsToString(permissions, grantResults));

        //neither permissions or grantResults should be empty but google docs warns they could be and should be treated as a cancellation
        if (permissions.length == 0 && grantResults.length == 0) {
            Logger.w("empty permissions and/or grantResults");
            return;
        }

        if (requestCode == PermissionsUtil.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_FILES) {
            if (PermissionsUtil.isGranted(grantResults, 0)) {
                //If the user granted us permission to file access, so send logs now
                BbmUtils.sendBbmLogFiles(getContext());
            } else {
                PermissionsUtil.displayCanNotContinueIfCanNotAsk(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        R.string.rationale_write_external_storage_denied);
            }
        }
    }

    private void startBBMEnterpriseService() {
        mSignInButton.setEnabled(false);  //disable button to avoid multiple clicks
        mSetupErrorView.setText(null); //clear any outstanding errors

        final boolean startSuccessful = mBbmEnterprise.start();
        if (!startSuccessful) {
            //implies BBMEnterprise was already started.  Call stop before trying to start again
            Logger.i("Service already started.");
        }
        updateServiceButtonState(false);  //show stop on the button
    }

    private void stopBBMEnterpriseService() {
        try {
            mBbmEnterprise.wipe();   //wipe credentials
        } catch (RuntimeException e) {
            Toast.makeText(this.getContext(), "Unable to wipe credentials.", Toast.LENGTH_LONG).show();
        }

        mBbmEnterprise.stop(); //stop the service
        updateServiceButtonState(true); // show the sign in button
    }


    private void updateServiceButtonState(final boolean stopped) {
        //update the click behavior
        mServiceStopButton.setVisibility(stopped ? View.GONE : View.VISIBLE);
        mSignInButton.setVisibility(stopped ? View.VISIBLE : View.GONE);

        mSignInButton.setEnabled(true);
        mServiceStopButton.setEnabled(true);
    }

    private final Observer mEnterpriseStateObserver = new Observer() {
        @Override
        public void changed() {
            final BBMEnterpriseState bbmEnterpriseState = mBbmEnterpriseState.get();

            Logger.i("bbmEnterpriseState: " + bbmEnterpriseState.toString());
            mEnterpriseStateView.setText(bbmEnterpriseState.toString());

            //Show the failure reason if we are in the failure state
            mServiceFailureContainer.setVisibility(
                    bbmEnterpriseState == BBMEnterpriseState.FAILED ?
                    View.VISIBLE : View.GONE);

            //Show the restart button if we are in the failure state
            mRestartButton.setVisibility(
                    bbmEnterpriseState == BBMEnterpriseState.FAILED ?
                    View.VISIBLE : View.GONE);
            switch (bbmEnterpriseState) {

                case UNINITIALIZED:
                    updateServiceButtonState(true);
                    break;
                case STARTING:
                case STARTED:
                    updateServiceButtonState(false);
                    break;
                case STOPPED:
                    mSetupStateView.setText("");
                    mAuthTokenStateView.setText("");
                    mLocalPinView.setText("");
                    mLocalRegIdView.setText("");
                    updateServiceButtonState(true);
                    break;
                case FAILED:
                    //Get the failure reason
                    mServiceFailureContainer.setVisibility(View.VISIBLE);
                    mServiceFailureView.setText(mBbmEnterprise.getFailureReason().toString());
                default:
                    updateServiceButtonState(true);
                    break;
            }
        }
    };


    private final Observer mSetupStateObserver = new Observer() {
        @Override
        public void changed() {
            final GlobalSetupState setupState = mSetupState.get();
            mSetupStateView.setText(setupState.state.toString());

            if (setupState.getExists() != Existence.YES) {
                return;
            }

            switch (setupState.state) {
                case NotRequested:
                    break;
                case DeviceSwitchRequired:
                    break;
                case Ongoing:
                    //Ongoing has additional information in the progressMessage
                    mSetupStateView.setText(setupState.state.toString() + ":" + setupState.progressMessage.toString());
                    break;
                case Success:
                    break;
                case Unspecified:
                    break;
            }
        }
    };

    private final Observer mAuthTokenStateObserver = new Observer() {

        @Override
        public void changed() {
            final GlobalAuthTokenState authTokenState = mAuthTokenState.get();
            mAuthTokenStateView.setText(authTokenState.value.toString());

            if (authTokenState.getExists() != Existence.YES) {
                return;
            }

            switch (authTokenState.value) {
                case Ok:
                    mRetryButton.setVisibility(View.GONE);
                    mSetupErrorContainer.setVisibility(View.GONE);
                    break;
                case Needed:
                    mRetryButton.setVisibility(View.GONE);
                    mSetupErrorContainer.setVisibility(View.GONE);
                    break;
                case Rejected:
                    mRetryButton.setVisibility(View.VISIBLE);
                    mSetupErrorContainer.setVisibility(View.VISIBLE);
                    mSetupErrorView.setText(R.string.token_rejected_error);
                    break;
                case Unspecified:
                    break;
            }
        }
    };

    private final Observer mLocalPinObserver = new Observer() {
        @Override
        public void changed() {
            final String value = mLocalPin.get().value;
            mLocalPinView.setText(value);
        }
    };


    private final Observer mLocalBbmUserObserver = new Observer() {
        @Override
        public void changed() {
            final long value = mLocalBbmUser.get().regId;
            mLocalRegIdView.setText(String.valueOf(value));
        }
    };

    private final Observer mLocalAppUserObserver = new Observer() {
        @Override
        public void changed() {
            if (mLocalAppUser.get().getExists() == Existence.YES) {
                mGoogleEmailView.setText(mLocalAppUser.get().getEmail());
                mGoogleNameView.setText(mLocalAppUser.get().getName());

                String avatarUrl = mLocalAppUser.get().getAvatarUrl();
                if (TextUtils.isEmpty(avatarUrl)) {
                    mGoogleAvatarView.setImageURI(null);
                } else {
                    ImageTask.load(avatarUrl, mGoogleAvatarView);
                }
            } else {
                mGoogleEmailView.setText("");
                mGoogleNameView.setText("");
                mGoogleAvatarView.setImageURI(null);
            }
        }
    };

    private final ProtocolMessageConsumer mSetupErrorListener = new ProtocolMessageConsumer() {
        @Override
        public void onMessage(final ProtocolMessage message) {
            final String type = message.getType();
            if (type.equals("setupError")) {
                final String errorValue = message.getData().optString("error");
                mSetupErrorContainer.setVisibility(View.VISIBLE);
                mSetupErrorView.setText(errorValue);
            }
        }

        @Override
        public void resync() {
            //intentionally blank
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();

        //need to force changed to run to update UI in case the value doesn't change right away
        mEnterpriseStateObserver.changed();
        mSetupStateObserver.changed();
        mAuthTokenStateObserver.changed();
        mLocalPinObserver.changed();
        mLocalBbmUserObserver.changed();
        mLocalAppUserObserver.changed();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mSetupErrorContainer.setVisibility(View.VISIBLE);
        mSetupErrorView.setText(connectionResult.getErrorMessage());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.d("requestCode="+requestCode+" resultCode="+resultCode+" data="+data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            onSilentSignResult(result);
            Logger.d("AUTH: Google result :" + result.toString());
        }
    }

    @Override
    public void onSuccess(@NonNull AuthToken token) {
        // It is possible the fragment can be removed when this method is called,
        // so check and leave if so.
        if (GoogleConnectSetupFragment.this.getActivity() == null ||
                GoogleConnectSetupFragment.this.isDetached() ||
                GoogleConnectSetupFragment.this.isRemoving()) {
            return;
        }

        startBBMEnterpriseService();
        mAuthTokenStateObserver.changed();
    }

    @Override
    public void onFail() {
        mSetupErrorContainer.setVisibility(View.VISIBLE);
        mSetupErrorView.setText(R.string.google_sign_failed);
    }

    @Override
    public void onSilentSignResult(@NonNull GoogleSignInResult googleSignInResult) {
        Logger.d("googleSignInResult="+googleSignInResult);

        // It is possible the fragment can be removed when this method is called,
        // so check and leave if so.
        if (GoogleConnectSetupFragment.this.getActivity() == null ||
                GoogleConnectSetupFragment.this.isDetached() ||
                GoogleConnectSetupFragment.this.isRemoving()) {

            Logger.d("returning, activity="+GoogleConnectSetupFragment.this.getActivity());
            return;
        }

        if (googleSignInResult.isSuccess()) {
            Logger.d("success");
            mSetupErrorContainer.setVisibility(View.GONE);
            mSetupErrorView.setText("");

            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = googleSignInResult.getSignInAccount();
            if (acct != null) {
                mGoogleEmailView.setText(acct.getEmail());
                mGoogleNameView.setText(acct.getDisplayName());
                ImageTask.load(acct.getPhotoUrl().toString(), mGoogleAvatarView);

                Uri photoUrl = acct.getPhotoUrl();
                String photoUrlString = null;
                if (photoUrl != null) {
                    photoUrlString = photoUrl.toString();
                }

                AuthenticatedAccountData accountData = new AuthenticatedAccountData(acct.getId(), acct.getIdToken(),
                        acct.getDisplayName(), acct.getEmail(), photoUrlString);

                FirebaseUserDbSync.getInstance().localAuthDataChanged(accountData);

                GoogleGetAccessTokenTask task = new GoogleGetAccessTokenTask(this.getActivity(), acct, this);
                task.execute();
            } else {
                mSetupErrorContainer.setVisibility(View.VISIBLE);
                mSetupErrorView.setText(R.string.google_signed_in_no_acct);
                mGoogleEmailView.setText("");
                mGoogleNameView.setText("");
                mGoogleAvatarView.setImageURI(null);
            }
        } else {

            Logger.d("failed getSignInAccount="+googleSignInResult.getSignInAccount()
            +" getStatus="+googleSignInResult.getStatus()
                    +" isSuccess="+googleSignInResult.isSuccess());

            mSetupErrorContainer.setVisibility(View.VISIBLE);
            mSetupErrorView.setText(R.string.google_sign_failed);
            mGoogleEmailView.setText("");
            mGoogleNameView.setText("");
            mGoogleAvatarView.setImageURI(null);
        }
    }
}
