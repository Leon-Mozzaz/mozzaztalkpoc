/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;


import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bbm.example.common.ui.messages.MessageHelper;
import com.bbm.sdk.support.util.Logger;
import com.bbm.example.common.util.Utils;
import com.bbm.sdk.support.reactive.ObserveConnector;
import com.bbm.example.richchat.util.RichChatUtils;
import com.bbm.sdk.reactive.Mutable;
import com.bbm.sdk.reactive.Observer;

/**
 * The BBM text input control that includes an input bar and buttons
 */
public final class EmoticonInputPanel extends LinearLayout {
    public enum Mode {
        Keyboard,
        None, KeyboardShowing,
        Attachment}

    private SendEditText mMessageInput;

    private Mode mMode = Mode.None;

    private boolean mKeyboardEnterAsNewLineAllowed = false;
    private boolean mHasSecondaryBar = false;

    private MessageHelper.MessagePriorityLevel messagePriorityLevel = MessageHelper.MessagePriorityLevel.NORMAL;

    private final ImageView mSendButton;
    private final ImageButton mAttachFileButton;
    private final ImageButton mPriorityButton;
    private final View mSecondaryRow;
    private final FrameLayout mAttachmentPane;


    private Mutable<Boolean> mHasText = new Mutable<>(false);
    private Mutable<Boolean> mHasAttachment = new Mutable<>(false);

    private Observer mSendButtonObserver = ObserveConnector.addObserver(new Observer() {
        @Override
        public void changed() {
            if (!mHasText.get() && !mHasAttachment.get()) {
                setSendButtonClickable(false);
            } else {
                setSendButtonClickable(true);
            }
        }
    }, mHasText, mHasAttachment);

    private OnActionClickedListener mOnActionClickedListener;

    public interface OnActionClickedListener {
        boolean onPictureClicked();

        void onCameraClicked();

        void onFileClicked();


        void send();
    }

    private TextWatcher mInputWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            mHasText.set(!TextUtils.isEmpty(s.toString()));
        }
    };


    private OnClickListener mPriorityClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            messagePriorityLevel = messagePriorityLevel.next();

            RichChatUtils.showDefaultToast(getContext(), getContext().getString(R.string.message_priority_set, messagePriorityLevel));

            switch (messagePriorityLevel) {
                case LOW:
                    mPriorityButton.setImageResource(R.drawable.ic_qs_priority_low_selected);
                    break;
                case NORMAL:
                    mPriorityButton.setImageResource(R.drawable.ic_qs_priority_normal_selected);
                    break;
                case HIGH:
                    mPriorityButton.setImageResource(R.drawable.ic_qs_priority_high_selected);
                    break;
            }
        }
    };

    public EmoticonInputPanel(final Context context) {
        this(context, null);
    }

    public EmoticonInputPanel(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EmoticonInputPanel(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        LayoutInflater.from(context).inflate(R.layout.view_bbm_input_panel, this);

        setBackgroundResource(R.color.conversation_input_background);
        setFocusable(true);
        setFocusableInTouchMode(true);

        mAttachmentPane = (FrameLayout) findViewById(R.id.attachment_pane);

        mSendButton = (ImageView) findViewById(R.id.send_button);
        ImageButton attachPictureButton = (ImageButton) findViewById(R.id.attach_picture);
        ImageButton cameraButton = (ImageButton) findViewById(R.id.attach_camera);
        mAttachFileButton = (ImageButton) findViewById(R.id.attach_file);
        mSecondaryRow = findViewById(R.id.extended_row);
        mPriorityButton = (ImageButton) findViewById(R.id.message_priority_button);



        mSendButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (mOnActionClickedListener != null) {
                    mOnActionClickedListener.send();
                }
            }
        });

        mPriorityButton.setOnClickListener(mPriorityClickListener);

        attachPictureButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideLowerPanelIfVisible();
                if (mOnActionClickedListener != null) {
                    mOnActionClickedListener.onPictureClicked();
                }
            }
        });
        cameraButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.cameraAvailable(getContext())) {
                    RichChatUtils.showDefaultToast(getContext(), getResources().getString(R.string.msg_no_camera_detected), Snackbar.LENGTH_SHORT);
                    return;
                }
                hideLowerPanelIfVisible();
                if (mOnActionClickedListener != null) {
                    mOnActionClickedListener.onCameraClicked();
                }
            }
        });
        mAttachFileButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideLowerPanelIfVisible();
                if (mOnActionClickedListener != null) {
                    mOnActionClickedListener.onFileClicked();
                }
            }
        });

        mMessageInput = (SendEditText) findViewById(R.id.message_input_text);
        mMessageInput.setKeyboardEnterAsNewLineOverrideAllowed(mKeyboardEnterAsNewLineAllowed);
        mMessageInput.setNextFocusLeftId(getId());
        mMessageInput.setNextFocusRightId(getId());
        mMessageInput.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    setLowerPanel(Mode.Keyboard);
                }
                return false;
            }
        });
        mMessageInput.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(final View v, final int keyCode, final KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (mKeyboardEnterAsNewLineAllowed) {
                            // return false will result in a \n added into the text box, and this is the only
                            // case that should happen
                            return false;
                        } else {
                            if (mOnActionClickedListener != null) {
                                mOnActionClickedListener.send();
                            }
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        final TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.EmoticonInputPanel, defStyle,
                0);
        try {
            final CharSequence hint = a.getText(R.styleable.EmoticonInputPanel_inputHint);
            mMessageInput.setHint(hint);
            mMessageInput.addTextChangedListener(mInputWatcher);
            mHasSecondaryBar = a.getBoolean(R.styleable.EmoticonInputPanel_hasSecondaryBar, false);
            mSecondaryRow.setVisibility(mHasSecondaryBar ? View.VISIBLE : View.GONE);
            if (mHasSecondaryBar) {
                boolean hasPicture = a.getBoolean(R.styleable.EmoticonInputPanel_hasPicture, false);
                boolean hasCamera = a.getBoolean(R.styleable.EmoticonInputPanel_hasCamera, false);
                boolean hasFile = a.getBoolean(R.styleable.EmoticonInputPanel_hasFile, false);
                attachPictureButton.setVisibility(hasPicture ? View.VISIBLE : View.GONE);
                cameraButton.setVisibility(hasCamera ? View.VISIBLE : View.GONE);
                mAttachFileButton.setVisibility(hasFile ? View.VISIBLE : View.GONE);
            }

        } finally {
            a.recycle();
        }
        clearButtonSelected();
        mSendButtonObserver.changed();
    }

    /**
     * Override of the removeAllViews, needed as issue with AppCompat-v7:22 lib is causing
     * a crash of the application on close. This is issue is triggered deep in the google code
     * which bubbles up. To be safe we are overriding and trapping the exception to continue
     * to work.
     */
    @Override
    public void removeAllViews() {
        try {
            super.removeAllViews();
        } catch (final Exception e) {
            Logger.e(e, "EmoticonInputPanel - removing views crash");
        }
    }

    /**
     * Override of the removeAllViewsInLayout, needed as issue with AppCompat-v7:22 lib is causing
     * a crash of the application on close. This is issue is triggered deep in the google code
     * which bubbles up. To be safe we are overriding and trapping the exception to continue
     * to work.
     */
    @Override
    public void removeAllViewsInLayout() {
        try {
            super.removeAllViewsInLayout();
        } catch (final Exception e) {
            Logger.e(e, "EmoticonInputPanel - removing all views in layout crash");
        }
    }

    private void clearButtonSelected() {
        mSendButton.setSelected(false);
    }




    public void addAttachmentView(final AttachmentView attachmentView) {
        if (mAttachmentPane != null) {
            mAttachmentPane.removeAllViews();
            mAttachmentPane.setVisibility(VISIBLE);
            mAttachmentPane.addView(attachmentView);
        }
        mHasAttachment.set(true);
    }

    public void removeAttachementView() {
        if (mAttachmentPane != null) {
            mAttachmentPane.removeAllViews();
            mAttachmentPane.setVisibility(GONE);
        }
        mHasAttachment.set(false);
        resetMessageFlags();
    }

    public void setOnActionClickedListener(final OnActionClickedListener onActionClickedListener) {
        mOnActionClickedListener = onActionClickedListener;
    }

    public boolean isLowerPanelVisible() {
        return mMode != Mode.None;
    }

    public boolean hideLowerPanelIfVisible() {
        if (mMode != Mode.None) {
            setLowerPanel(Mode.None);
            return true;
        }
        return false;
    }

    /**
     * This should be the single entry point to toggle between different modes.
     * KeyboardShowing is an intermediate state that keyboard is requested but not finished its
     * transition to fully show on screen. DO NOT use KeyboardShowing.
     * DO NOT call InputMethodManager.showSoftInput() directly from the activity. Instead always
     * use setLowerPanel(com.bbm.ui.EmoticonInputPanel.Mode) with Mode.Keyboard;
     */
    public void setLowerPanel(Mode mode) {

        if (mMode == mode) {
            return;
        }
        final Activity activity = (Activity) getContext();

        if (activity == null) {
            return;
        }

        if (mode == Mode.Keyboard && Utils.hasHardwareKeyboard(activity)) {
            mode = Mode.None;
        }

        final boolean hasHardKeyboard = Utils.hasHardwareKeyboard(activity);

        Logger.d("EmotionInputPanel.setLowerPanel: new mode=" + mode + " old mode=" + mode + " hasHardKeyboard=" + hasHardKeyboard + " B=" + Build.BRAND);

        final InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        Mode oldMode = mMode;
        mMode = mode;
        if (oldMode == Mode.Keyboard || oldMode == Mode.KeyboardShowing) {
            if (mode != Mode.None || !hasHardKeyboard) {
                //Either switching to a mode where we display something in lower panel (not none) or there isn't a hard keyboard
                //hide virtual keyboard when switching to modes where we show something in lower panel
                //where keyboard would be (timed messages, glympse...) but not when switching to none mode if hard keyboard since user
                //might keep typing with a physical keyboard and we want to allow virtual keyboard to appear again
                imm.hideSoftInputFromWindow(getWindowToken(), 0);
            }
        }
        clearButtonSelected();
        switch (mode) {
            case Keyboard:
                mMessageInput.requestFocus();
                imm.restartInput(mMessageInput);
                imm.showSoftInput(mMessageInput, 0);
                mMode = Mode.KeyboardShowing;
                activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                break;
            case None:
                activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

                if (hasHardKeyboard && oldMode != Mode.Keyboard && oldMode != Mode.KeyboardShowing) {
                    //we probably hid the virtual keyboard when switching to other modes so we should allow it to show again now if needed
                    imm.showSoftInput(mMessageInput, 0);
                }

                break;

            default:
                break;
        }
    }


    @Override
    public boolean dispatchKeyEventPreIme(@NonNull final KeyEvent event) {
        final InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        //if this is the back key we hide the lower panel unless it is the keyboard mode, then we use the default handling
        if (imm.isActive() && event.getKeyCode() == KeyEvent.KEYCODE_BACK && isLowerPanelVisible() && mMode != Mode.Keyboard) {
            hideLowerPanelIfVisible();
            return true;
        }
        return super.dispatchKeyEventPreIme(event);
    }

    public SendEditText getMessageInput() {
        return mMessageInput;
    }

    /**
     * Clear any existing message flag, currently this resets the priority message state
     */
    public void resetMessageFlags() {
        //Clear priority flag
        messagePriorityLevel = MessageHelper.MessagePriorityLevel.NORMAL;
        mPriorityButton.setImageResource(R.drawable.ic_qs_priority_normal_selected);
    }

    public MessageHelper.MessagePriorityLevel getPriority() {
        return messagePriorityLevel;
    }

    /**
     * Set the clickable state of the Send button. If the user has set allowEmptyText, then
     * this setting is ignored and the button is by default clickable.
     */
    public void setSendButtonClickable(final boolean clickable) {
        if (mSendButton != null) {
            if (clickable) {
                mSendButton.setVisibility(VISIBLE);
            } else {
                mSendButton.setVisibility(INVISIBLE);
            }
        }
    }
}
