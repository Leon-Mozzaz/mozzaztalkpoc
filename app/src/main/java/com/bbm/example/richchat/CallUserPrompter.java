/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.bbm.example.richchat.util.RichChatUtils;
import com.bbm.sdk.support.identity.user.AppUser;
import com.bbm.sdk.support.identity.user.UserManager;
import com.bbm.sdk.support.util.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * Simple prompt for PIN to start a call.
 */
public class CallUserPrompter {

    private static final String PIN_REGEX = "[0-9A-Fa-f]{8}";

    private Activity mActivity;

    private AutoCompleteTextView mRegIdInputEditText;
    private SelectedRegIdCallback mCallback;

    public interface SelectedRegIdCallback {
        void selectedRegId(long regId);
    }

    public static void promptToStartCall(Activity activity, SelectedRegIdCallback callback) {
        new CallUserPrompter(activity, callback);
    }

    private CallUserPrompter(Activity activity, SelectedRegIdCallback callback) {
        mActivity = activity;
        mCallback = callback;

        LayoutInflater inflater = mActivity.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_message_with_input, null);

        dialogView.findViewById(R.id.user_list).setVisibility(View.GONE);
        ((TextView)dialogView.findViewById(R.id.dialog_info_first_line)).setText(R.string.regid_to_call);
        dialogView.findViewById(R.id.dialog_info_second_line).setVisibility(View.GONE);
        dialogView.findViewById(R.id.dialog_input_value2).setVisibility(View.GONE);

        mRegIdInputEditText = (AutoCompleteTextView) dialogView.findViewById(R.id.dialog_input_value);
        mRegIdInputEditText.setThreshold(1);
        setAdapter();

        mRegIdInputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);

        final AlertDialog dialog = new AlertDialog.Builder(mActivity, R.style.BBMAppTheme_dialog)
                .setTitle(R.string.start_call)
                .setView(dialogView)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.start_call, null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface d) {

                Window window = dialog.getWindow();
                WindowManager.LayoutParams layoutParams = window.getAttributes();
                layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
                window.setAttributes(layoutParams);

                mRegIdInputEditText.showDropDown();

                //set button listener this way so the dialog stays open until we have valid PIN and dismiss it
                dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        long regId = parseRegId(mRegIdInputEditText.getText().toString());
                        if (regId != -1) {
                            mCallback.selectedRegId(regId);
                            dialog.dismiss();
                        }
                    }
                });
            }
        });

        dialog.show();
    }

    private void setAdapter() {
        List<AppUser> otherUsers = UserManager.getInstance().getUsers().get();
        ArrayList<String> userInfo = new ArrayList();
        for (int i=0;i<otherUsers.size();++i) {
            //skip this if this user will already be included
            AppUser user = otherUsers.get(i);
            userInfo.add(user.getRegId() + "\n (" + user.getName() + ")");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(mActivity, R.layout.autocomplete_item, R.id.autocompleteTextView, userInfo);
        mRegIdInputEditText.setAdapter(adapter);
    }

    private long parseRegId(String regIdString) {
        if (!TextUtils.isEmpty(regIdString)) {
            try {
                int sep = regIdString.indexOf(' ');
                if (sep > 0) {
                    regIdString = regIdString.substring(0, sep).trim();
                }
                long regId = Long.parseLong(regIdString);
                return regId;
            } catch (NumberFormatException ne) {
                Logger.i(ne, "Invalid RegId=" + regIdString);
                RichChatUtils.showDefaultToast(mActivity, "Invalid RegId=" + regIdString);
            }
        }
        return -1;
    }
}
