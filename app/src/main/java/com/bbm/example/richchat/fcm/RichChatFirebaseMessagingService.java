package com.bbm.example.richchat.fcm;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.bbm.example.richchat.BuildConfig;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.service.BBMEnterpriseState;
import com.bbm.sdk.support.util.Logger;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Set;

public class RichChatFirebaseMessagingService extends FirebaseMessagingService {

    private Handler mMainHandler = new Handler(Looper.getMainLooper());

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Logger.d("onMessageReceived: "+remoteMessage);
        if (remoteMessage.getFrom().equals(BuildConfig.CLOUD_MESSAGING_SENDER_ID)) {
            // We convert the remote message into a bundle to be able to pass it to the SDK
            final Bundle bundle = new Bundle();
            final Set<Map.Entry<String, String>> data = remoteMessage.getData().entrySet();
            for (Map.Entry<String, String> entry : data) {
                bundle.putString(entry.getKey(), entry.getValue());
            }

            // Handling of push notification must be done of UI thread.
            mMainHandler.post(new Runnable() {
                @Override
                public void run() {
                    Logger.i("Handle incoming push.");
                    try {
                        try {
                            BBMEnterpriseState state = BBMEnterprise.getInstance().getState().get();
                            Logger.d("BBMEnterpriseState="+state);
                            if (state != BBMEnterpriseState.STARTED) {
                                Logger.i("Starting BBM SDK before handling push");
                                BBMEnterprise.getInstance().start();
                            }
                            
                            BBMEnterprise.getInstance().handlePushNotification(bundle);
                        } catch (IllegalStateException e) {
                            //This should not happen, but init and start BBM SDK first before handle push
                            Logger.w(e, "Restarting BBM Service to handle push.");
                            BBMEnterprise mBBMEnterprise = BBMEnterprise.getInstance();
                            // BBMEnterprise needs to be initialized and started
                            mBBMEnterprise.initialize(getApplicationContext());
                            mBBMEnterprise.start();
                            mBBMEnterprise.handlePushNotification(bundle);
                        }
                    } catch (Exception e) {
                        // Failed to process Push
                        Logger.e(e, "Failed to process push");
                    }


                    Logger.d("Done handle incoming push.");
                }
            });
        }
    }
}