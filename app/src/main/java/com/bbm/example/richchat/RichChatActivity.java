/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.Manifest;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import com.bbm.example.common.ui.AppUserSelectedCallback;
import com.bbm.example.common.ui.ChooseAppUserDialog;
import com.bbm.example.common.ui.messages.ChatMessageRecyclerViewAdapter;
import com.bbm.example.common.ui.messages.DecoratedMessage;
import com.bbm.example.common.ui.messages.FileUtil;
import com.bbm.example.common.ui.messages.MessageHelper;
import com.bbm.example.common.ui.messages.RecyclerContextMenuInfoWrapperView;
import com.bbm.example.common.util.PermissionsUtil;
import com.bbm.example.common.util.Utils;
import com.bbm.example.richchat.image.ImageUtil;
import com.bbm.example.richchat.media.MediaUtils;
import com.bbm.example.richchat.util.RichChatUtils;
import com.bbm.example.richchat.util.StringUtil;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.bbmds.Chat;
import com.bbm.sdk.bbmds.ChatMessage;
import com.bbm.sdk.bbmds.Participant;
import com.bbm.sdk.bbmds.ParticipantCriteria;
import com.bbm.sdk.bbmds.TypingUser;
import com.bbm.sdk.bbmds.User;
import com.bbm.sdk.bbmds.internal.Existence;
import com.bbm.sdk.bbmds.outbound.ChatMessageSend;
import com.bbm.sdk.bbmds.outbound.ChatShred;
import com.bbm.sdk.bbmds.outbound.DeleteMessage;
import com.bbm.sdk.bbmds.outbound.RetryServerRequests;
import com.bbm.sdk.bbmds.outbound.StopConversation;
import com.bbm.sdk.common.IOHelper;
import com.bbm.sdk.reactive.ObservableValue;
import com.bbm.sdk.reactive.Observer;
import com.bbm.sdk.reactive.SingleshotMonitor;
import com.bbm.sdk.reactive.StateAwareList;
import com.bbm.sdk.support.identity.user.AppUser;
import com.bbm.sdk.support.identity.user.AppUserFilter;
import com.bbm.sdk.support.identity.user.UserManager;
import com.bbm.sdk.support.protect.ProtectedManager;
import com.bbm.sdk.support.reactive.ArrayObservableList;
import com.bbm.sdk.support.reactive.ObserveConnector;
import com.bbm.sdk.support.util.BbmUtils;
import com.bbm.sdk.support.util.IOUtils;
import com.bbm.sdk.support.util.Logger;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class RichChatActivity extends AppCompatActivity implements OpenCameraForResult.Callback {
    public static final int REQUEST_CODE_ATTACH_PICTURE = 5;
    public static final int REQUEST_CODE_ATTACH_FILE = 6;
    public static final int REQUEST_CODE_CAMERA = 7;

    public static final String EXTRA_CHAT_ID = "chat-id";

    private static final int TYPING_NOTIFICATION_DELAY = 30000;

    private String mChatId;

    private ObservableValue<Chat> mChat;

    private StateAwareList<Participant> mParticipants;

    /**
     * The 1st user in the chat, will be null until needed
     */
    private ObservableValue<User> mRemoteUser;
    private ObservableValue<TypingUser> mTypingUser;

    private AlertDialog mDeleteMessageDialog;

    private final SingleshotMonitor mUpdateDraftMessageMonitor = new SingleshotMonitor() {

        @Override
        protected boolean runUntilTrue() {
            final Chat chat = mChat.get();
            if (chat.exists == Existence.YES) {
                String message = BbmUtils.getDraftMessage(chat);
                long viewTime = BbmUtils.getDraftViewTime(chat);
                if (message == null) {
                    return true;
                }

                if (!TextUtils.isEmpty(message)
                        && !TextUtils.equals(message, getMessageInput().getText())) {
                    getMessageInput().setText("");
                    // The text is appended so that cursor is placed at the end
                    getMessageInput().append(message);
                }

                return true;
            }

            return false;
        }

    };

    private ObserveConnector mObserveConnector = new ObserveConnector();

    private ChatMessageRecyclerViewAdapter mListAdapter;

    private TypingNotificationHelper mTypingNotificationHelper;


    private EmoticonInputPanel mEmoticonInputPanel;
    private EmoticonPanelViewLayout mRootView;
    private SendEditText mMessageInput;
    private TextView mHeaderTitle;
    private TextView mStatusMessage;

    private RecyclerView mListMessages;
    private LinearLayoutManager mLinearLayoutManager;
    private boolean mListViewScrolledByUser = false;
    private final Handler mHandler = new Handler();

    private int mPreviousRequestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
    // Last message which we processed from the user text input
    private String mPrevString;

    private AttachmentView mAttachmentView;
    private String mAttachFileTransferFilePath = null;
    private Uri mCameraFileUri;
    private boolean mIsAttachmentPreremoved = false;

    private String mContextContentId;
    private String mContextUserUri;
    private String mContextContentType;
    private OpenCameraForResult mOpenCameraForResult;

    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                mListViewScrolledByUser = true;
            } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                if (mListViewScrolledByUser && isLastItemVisible()) {
                    BbmUtils.markChatMessagesAsRead(mChatId);
                }
                mListViewScrolledByUser = false;
            }
        }
    };

    private final EmoticonInputPanel.OnActionClickedListener mOnActionClickedListener =
            new EmoticonInputPanel.OnActionClickedListener() {

                @Override
                public boolean onPictureClicked() {
                    return attachFromGallery();
                }

                @Override
                public void onCameraClicked() {
                    if (mOpenCameraForResult == null) {
                        mOpenCameraForResult = new OpenCameraForResult(RichChatActivity.this, RichChatActivity.REQUEST_CODE_CAMERA);
                        mHandler.post(mOpenCameraForResult);
                    }
                }

                @Override
                public void onFileClicked() {
                    attachFromFilePicker();
                }

                @Override
                public void send() {
                    onSendClicked();
                }
            };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mChatId = getIntent().getStringExtra(EXTRA_CHAT_ID);
        mChat = BBMEnterprise.getInstance().getBbmdsProtocol().getChat(mChatId);
        mParticipants = BBMEnterprise.getInstance().getBbmdsProtocol().getParticipantList(new ParticipantCriteria().conversationUri(BbmUtils.chatIdToConversationUri(mChatId)));

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_conversation);
        getWindow().setBackgroundDrawable(null);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            actionBar.setCustomView(R.layout.view_actionbar_contact_status);
            View customActionBarView = actionBar.getCustomView();
            mHeaderTitle = (TextView) findViewById(R.id.actionbar_name);
            mStatusMessage = (TextView) customActionBarView.findViewById(R.id.actionbar_status_message);
            actionBar.getCustomView().getLayoutParams().width = ActionBar.LayoutParams.MATCH_PARENT;
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }


        mListMessages = (RecyclerView) findViewById(R.id.list_messages);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mListMessages.setLayoutManager(mLinearLayoutManager);

        // Initialize messages adapter
        mListMessages.setAdapter(getListAdapter());
        mListMessages.addOnScrollListener(mScrollListener);

        mEmoticonInputPanel = (EmoticonInputPanel) findViewById(R.id.emoticon_input_panel);
        mEmoticonInputPanel.setOnActionClickedListener(mOnActionClickedListener);

        mMessageInput = mEmoticonInputPanel.getMessageInput();
        mMessageInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isLastItemVisible()) {
                    BbmUtils.markChatMessagesAsRead(mChatId);
                }

                // String comparison is done here to avoid calling notifyUserTyping() when sending an attachment.
                // When sending a picture, the text box is cleared, thus onTextChanged() is triggered.
                if (!TextUtils.equals(mPrevString, s.toString()) && !TextUtils.isEmpty(s.toString())
                        && !isLocalUserTyping()) {
                    if (mTypingNotificationHelper != null) {
                        mTypingNotificationHelper.notifyUserTyping();
                    }
                }

                mPrevString = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mRootView = (EmoticonPanelViewLayout) findViewById(R.id.conversation_root);

        mRootView.setOnRootTouchListener(new EmoticonPanelViewLayout.BubbleListTouchListener(mListMessages) {
            @Override
            public void onDown() {
                if (isLastItemVisible()) {
                    BbmUtils.markChatMessagesAsRead(mChatId);
                }
            }

            @Override
            public void onUp() {
            }

            @Override
            public void onTap() {
                Logger.gesture("ListView tapped to dismiss keyboard", RichChatActivity.class);
                mEmoticonInputPanel.hideLowerPanelIfVisible();
                mMessageInput.clearFocus();
            }
        });

        mTypingNotificationHelper = new TypingNotificationHelper(BbmUtils.chatIdToConversationUri(mChatId));

        mObserveConnector.connectMultiple(new Observer() {
            @Override
            public void changed() {
                if (mChat != null && mChat.get().getExists() == Existence.YES) {
                    //we can't set mRemoteUser until mParticipants contains something
                    if (mRemoteUser == null && mParticipants.get().size() > 0) {
                        mRemoteUser = BBMEnterprise.getInstance().getBbmdsProtocol().getUser(mParticipants.get().get(0).userUri);
                        //also need to listen to this user in case they change name
                        mRemoteUser.addObserver(this);
                    }

                    if (!TextUtils.isEmpty(mChat.get().subject)) {
                        //use subject when set
                        setTitle(mChat.get().subject);
                    } else {
                        //get a title using chat participants names, and listen for updates to update UI
                        final ObservableValue<String> titleObservable = Utils.getFormattedParticipantList(mChatId);
                        mObserveConnector.connect(titleObservable, new Observer() {
                            @Override
                            public void changed() {
                                String title = titleObservable.get();
                                if (TextUtils.isEmpty(title)) {
                                    title = getString(R.string.chats_empty_chat);
                                }
                                setTitle(title);
                            }
                        }, true); //call changed right away
                    }

                    String subTitle = "";

                    if (is1to1Conversation()) {
                        if (mRemoteUser != null && mRemoteUser.get().getExists() == Existence.YES) {
                            if (mTypingUser == null) {
                                final TypingUser.TypingUserKey lookupKey = new TypingUser.TypingUserKey(mRemoteUser.get().uri, BbmUtils.chatIdToConversationUri(mChatId));
                                mTypingUser = BBMEnterprise.getInstance().getBbmdsProtocol().getTypingUser(lookupKey);
                                //need to listen to it for changes
                                mTypingUser.addObserver(this);
                            }

                            if (mTypingUser.get().exists == Existence.YES) {
                                subTitle = getResources().getString(R.string.conversation_is_writing_a_message);
                            }
                        }
                    }

                    if (mStatusMessage != null) {
                        mStatusMessage.setText(subTitle);
                        if (TextUtils.isEmpty(subTitle)) {
                            mStatusMessage.setVisibility(View.GONE);
                        } else {
                            mStatusMessage.setVisibility(View.VISIBLE);
                        }
                    }
                }


            }
        }, true, //Call this right away to evaluate now and update UI as needed
        mChat, mParticipants); //listen to changes to the chat and participants list
    }


    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        if (mHeaderTitle != null) {
            mHeaderTitle.setText(title);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        RecyclerContextMenuInfoWrapperView.RecyclerContextMenuInfo info =
                (RecyclerContextMenuInfoWrapperView.RecyclerContextMenuInfo) menuInfo;
        int type = info.type;
        int position = info.position;
        int subIndex = info.contextViewIndex;

        DecoratedMessage decoratedMessage = mListAdapter.getItem(position);
        ChatMessage chatMessage = decoratedMessage.getChatMessage();
        final boolean isRecalled = ChatMessage.Recall.Recalled.equals(chatMessage.recall);

        ChatMessageRecyclerViewAdapter.BubbleType bubbleType = ChatMessageRecyclerViewAdapter.BubbleType.values()[type];

        if (!chatMessage.hasFlag(ChatMessage.Flags.Incoming) && isConference()) {
            menu.add(Menu.NONE, R.id.slide_menu_item_view_message_status, Menu.NONE, getString(R.string.context_menu_option_view_delivery_status));
        }

        switch (bubbleType) {
            //The below message types don't have a chat bubble look
            case ITEM_VIEW_TYPE_OTHER:
            case ITEM_VIEW_TYPE_DATA_NOT_AVAILABLE:
            case ITEM_VIEW_TYPE_DATA_DELETED:
                break;
            default:
                // those that should have "delete message"
                if (MessageHelper.isRetractable(chatMessage)) {
                    menu.add(Menu.NONE, R.id.slide_menu_item_options_retract_message,
                            Menu.NONE, R.string.positive_btn_delete_retract_confirmation);
                }

                menu.add(Menu.NONE, R.id.slide_menu_item_options_delete_message,
                        Menu.NONE, R.string.context_menu_option_delete);
                break;
        }

        switch (bubbleType) {
            case ITEM_VIEW_TYPE_TEXT_OUTGOING:
            case ITEM_VIEW_TYPE_TEXT_INCOMING:
                break;
            case ITEM_VIEW_TYPE_PICTURE_INCOMING:
            case ITEM_VIEW_TYPE_PICTURE_OUTGOING:
                if (subIndex == 1 && chatMessage.fileState == ChatMessage.FileState.Done) {
                    if (FileUtil.fileExists(chatMessage.file)) {
                        if (chatMessage.hasFlag(ChatMessage.Flags.Incoming)) {
                            menu.add(Menu.NONE, R.id.slide_menu_item_options_save_picture,
                                    Menu.NONE, R.string.context_menu_option_save_picture);
                        }
                        menu.setHeaderTitle(R.string.picture);
                    }
                }
                break;
        }

        // those that should have "copy text"
        switch (bubbleType) {
            case ITEM_VIEW_TYPE_PICTURE_INCOMING:
            case ITEM_VIEW_TYPE_PICTURE_OUTGOING:
                if (subIndex != 0) {
                    break;
                }
            case ITEM_VIEW_TYPE_FILE_TRANSFER_INCOMING:
            case ITEM_VIEW_TYPE_FILE_TRANSFER_OUTGOING:
            case ITEM_VIEW_TYPE_TEXT_OUTGOING:
            case ITEM_VIEW_TYPE_TEXT_INCOMING:
                menu.setHeaderTitle(R.string.message);
                if (!isRecalled) {
                    menu.add(Menu.NONE, R.id.slide_menu_item_options_copy_message, Menu.NONE, R.string.context_menu_option_copy);
                }
                break;
            default:
                break;
        }


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Logger.d("onContextItemSelected: "+item);

        int position = ((RecyclerContextMenuInfoWrapperView.RecyclerContextMenuInfo) item.getMenuInfo()).position;
        final ChatMessage chatMessage = mListAdapter.getItem(position).getChatMessage();
        switch (item.getItemId()) {
            case R.id.slide_menu_item_options_delete_message:
                mDeleteMessageDialog = MessageHelper.showMessageDeleteDialog(this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        if (which == Dialog.BUTTON_POSITIVE) {
                            BBMEnterprise.getInstance().getBbmdsProtocol().send(new DeleteMessage(BbmUtils.chatIdToConversationUri(mChatId), chatMessage.messageId));
                        }
                    }
                });
                return true;
            case R.id.slide_menu_item_options_retract_message:
                mDeleteMessageDialog = MessageHelper.showMessageRetractDialog(this, !isConference(),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                if (which == Dialog.BUTTON_POSITIVE) {
                                    MessageHelper.retractMessage(chatMessage, BbmUtils.chatIdToConversationUri(mChatId));
                                }
                            }
                        });
                return true;
            case R.id.slide_menu_item_options_copy_message:
                Logger.gesture("copy message", RichChatActivity.class);
                copyMessage(chatMessage);
                RichChatUtils.showDefaultToast(RichChatActivity.this, getString(R.string.message_copied));
                break;
            case R.id.slide_menu_item_options_save_picture:
                Logger.gesture("save picture", RichChatActivity.class);


                String suggestedFilename = MessageHelper.getSuggestedFileName(chatMessage);
                final String filename = (!TextUtils.isEmpty(suggestedFilename))
                        ? suggestedFilename : new File(chatMessage.file).getName();
                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(FileUtil.getFileExtension(filename));

                ImageUtil.saveImageToGallery(chatMessage.file, this, mimeType);

            break;
            case R.id.slide_menu_item_view_message_status:
                Logger.gesture("view message status", RichChatActivity.class);
                Intent partialStatusIntent = mListAdapter.getItem(position).getPartialStatusActivityIntent(RichChatActivity.this);
                Logger.d("position="+position+" partialStatusIntent="+partialStatusIntent);
                if (partialStatusIntent != null) {
                    startActivity(partialStatusIntent);
                }
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    protected ChatMessageRecyclerViewAdapter getListAdapter() {
        if (mListAdapter == null) {
            mListAdapter = new ChatMessageRecyclerViewAdapter(this, getListView(), mChatId);
        }
        return mListAdapter;
    }

    public boolean isLastItemVisible() {
        if (mListMessages != null && mLinearLayoutManager != null
                && getListAdapter() != null && getListAdapter().getItemCount() > 0) {
            try {
                final int lastVisible = mLinearLayoutManager.findLastVisibleItemPosition();
                if (lastVisible >= getListAdapter().getItemCount() - 1) {
                    return true;
                }
            } catch (Exception e) {
                // if the view is not laid out, findLastVisibleItemPosition throws an exception
                // but there is no way to check isLaidOut() prier to API level 19
                // in that case, silently ignore the exception and return false
            }
        }
        return false;
    }

    protected SendEditText getMessageInput() {
        return mMessageInput;
    }


    private final SingleshotMonitor mMarkMessagesAsReadMonitor = new SingleshotMonitor() {
        @Override
        protected boolean runUntilTrue() {
            if (!TextUtils.isEmpty(mChatId)) {
                BbmUtils.markChatMessagesAsRead(mChatId);
                return true;
            }
            return false;
        }
    };

    protected boolean isConference() {
        return !mChat.get().isOneToOne;
    }

    protected boolean is1to1Conversation() {
        return mChat.get().isOneToOne;
    }

    protected void sendTextMessage(String text) {
        JSONObject data = MessageHelper.setPriority(null, getEmoticonInputPanel().getPriority());
        ChatMessageSend messageSend = new ChatMessageSend(mChatId, ChatMessageSend.Tag.Text)
                .content(text)
                .data(data);

        Logger.d("sendMessage: sending "+messageSend);
        BBMEnterprise.getInstance().getBbmdsProtocol().send(messageSend);
    }



    protected void onSendClicked(){
        onSendClicked(true);
    }

    protected void onSendClicked(final boolean keepOriginal) {
        // Remove trailing whitespaces
        final String messageTrimmed = StringUtil.trimTrailingSpace(getMessageInput().getText().toString());

        if (!TextUtils.isEmpty(getAttachedFileTransferPath())) {
            sendFileTransfer(messageTrimmed, getAttachedFileTransferPath());
            setAttachedFileTransferPath(null);
            removeAttachment();
        }
        else if (!TextUtils.isEmpty(messageTrimmed)) {
            sendTextMessage(messageTrimmed);
        }
        getMessageInput().setText("");
        clearTypingNotification();
        jumpToBottomMessage();
        if (!TextUtils.isEmpty(mContextContentId)) {
            mContextContentId = "";
        }
        if (!TextUtils.isEmpty(mContextContentType)) {
            mContextContentType = "";
        }
        showAttachmentPane(false);

        //Reset timebomb/priority flags after sending the message
        getEmoticonInputPanel().resetMessageFlags();

        //Remove references of camera capture image
        mCameraFileUri = null;
    }

    protected void clearTypingNotification() {
        if (mTypingNotificationHelper != null) {
            mTypingNotificationHelper.resetHelper();
        }
    }

    protected void jumpToBottomMessage() {
        if (mListMessages != null) {
            int itemCount = getListAdapter().getItemCount();
            if (itemCount > 0) {
                mListMessages.scrollToPosition(itemCount - 1);
                BbmUtils.markChatMessagesAsRead(mChatId);
            }
        }
    }

    /**
     * Method to show the attachment pane and update related buttons' states
     */
    private void showAttachmentPane(final boolean show) {
        if (show) {
            getEmoticonInputPanel().addAttachmentView(mAttachmentView);
        } else {
            getEmoticonInputPanel().removeAttachementView();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getListAdapter().start();
        mUpdateDraftMessageMonitor.activate();

        //Allow BBM to retry any failed server messages
        BBMEnterprise.getInstance().getBbmdsProtocol().send(new RetryServerRequests());
        ProtectedManager.getInstance().retryFailedEvents();
    }

    @Override
    public void onPause() {
        super.onPause();
        getListAdapter().pause();
        mUpdateDraftMessageMonitor.dispose();
        final String message = StringUtil.trimTrailingSpace(getMessageInput().getText().toString());
        BbmUtils.saveDraftMessage(mChatId, message);

        mTypingNotificationHelper.stopNotifier();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mMarkMessagesAsReadMonitor.dispose();
        if (mDeleteMessageDialog != null) {
            mDeleteMessageDialog.dismiss();
            mDeleteMessageDialog = null;
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean result = super.onPrepareOptionsMenu(menu);

        return result;
    }

    protected RecyclerView getListView() {
        return mListMessages;
    }

    protected EmoticonInputPanel getEmoticonInputPanel() {
        return mEmoticonInputPanel;
    }

    protected Existence getConversationExists() {
        return mChat.get().exists;
    }


    protected boolean isNewConversation() {
        return mChat.get().numMessages == 0;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        Logger.d("onWindowFocusChanged: "+hasFocus);
        if (hasFocus) {
            mMarkMessagesAsReadMonitor.activate();
            getListAdapter().onLayoutComplete();
            SingleshotMonitor.run(new SingleshotMonitor.RunUntilTrue() {
                @Override
                public boolean run() {
                if (getConversationExists() == Existence.YES) {
                    if (!isConference() && (TextUtils.isEmpty(mContextUserUri) || TextUtils.isEmpty(mContextContentType))) {
                        getEmoticonInputPanel().hideLowerPanelIfVisible();
                    } else if (getMessageInput() != null) {
                        getMessageInput().requestFocus();
                    }
                    return true;
                }
                return getConversationExists() != Existence.MAYBE;
                }
            });
        }
    }

    private void copyMessage(final ChatMessage message) {
        String text = message.content;
        final ClipData clip = ClipData.newPlainText("simple text", text);
        Utils.safeClipBoardSetPrimaryClip(this, clip);
    }

    @Override
    public void OnCameraOpenTaskComplete(Uri uri) {
        Logger.d("OnCameraOpenTaskComplete: uri="+uri);
        mOpenCameraForResult = null;
        mCameraFileUri = uri;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Logger.d("onRequestPermissionsResult: requestCode=" + requestCode + " " + PermissionsUtil.resultsToString(permissions, grantResults));

        //neither permissions or grantResults should be empty but google docs warns they could be and should be treated as a cancellation
        if (permissions.length == 0 && grantResults.length == 0) {
            Logger.w("empty permissions and/or grantResults");
            return;
        }

        if (requestCode == PermissionsUtil.PERMISSION_CAMERA_REQUEST
                || requestCode == PermissionsUtil.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_CAMERA) {
            if (PermissionsUtil.isGranted(grantResults, 0)) {
                if (mOpenCameraForResult == null) {
                    mOpenCameraForResult = new OpenCameraForResult(this, REQUEST_CODE_CAMERA);
                    mHandler.post(mOpenCameraForResult);
                }
            } else {
                if (requestCode == PermissionsUtil.PERMISSION_CAMERA_REQUEST) {
                    PermissionsUtil.displayCanNotContinueIfCanNotAsk(this, Manifest.permission.CAMERA,
                            R.string.rationale_camera_denied);
                } else {
                    PermissionsUtil.displayCanNotContinueIfCanNotAsk(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            R.string.rationale_write_external_storage_denied);
                }
            }
        } else if (requestCode == PermissionsUtil.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_PICTURES) {
            if (PermissionsUtil.isGranted(grantResults, 0)) {
                attachFromGallery();
            } else {
                PermissionsUtil.displayCanNotContinueIfCanNotAsk(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        R.string.rationale_write_external_storage_denied);
            }
        } else if (requestCode == PermissionsUtil.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_FILES) {
            if (PermissionsUtil.isGranted(grantResults, 0)) {
                    attachFromFilePicker();
            } else {
                PermissionsUtil.displayCanNotContinueIfCanNotAsk(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        R.string.rationale_write_external_storage_denied);
            }
        } else if (requestCode == PermissionsUtil.PERMISSION_RECORD_AUDIO_FOR_VOICE_CALL) {
            if (PermissionsUtil.isGranted(grantResults, 0)) {
                startCall();
            } else {
                PermissionsUtil.displayCanNotContinueIfCanNotAsk(this, Manifest.permission.RECORD_AUDIO,
                        R.string.rationale_record_audio_denied);
            }
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        Logger.d("onActivityResult: " + resultCode + " req: " + requestCode + " data: " + data);

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }


        switch (requestCode) {
            case REQUEST_CODE_CAMERA:
                //the user could have denied storage permission while in camera app (removing camera permission then is ok)
                if (!PermissionsUtil.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    PermissionsUtil.displayCanNotContinue(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            R.string.rationale_write_external_storage_denied, PermissionsUtil.PERMISSION_CAMERA_REQUEST);
                    return;
                }

                if (mCameraFileUri != null) {
                    String filepath = mCameraFileUri.getPath();
                    onAttach();
                    getEmoticonInputPanel().hideLowerPanelIfVisible();
                    attachFileTransfer(filepath);
                }
                break;
            case REQUEST_CODE_ATTACH_PICTURE:
                if (data == null) {
                    return;
                }

                Uri imageUri = data.getData();
                if (imageUri != null) {
                    Cursor cursor = getContentResolver().query(imageUri, new String[] {MediaStore.Images.Media.DATA}, null, null, null);
                    if (cursor.moveToFirst()) {
                        int colIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                        String imagePath = cursor.getString(colIndex);

                        Logger.d("onActivityResult: imagePath="+imagePath+" i="+colIndex+" cur="+cursor);
                        onAttach();
                        attachFileTransfer(imagePath);
                    }
                }


                break;
            case REQUEST_CODE_ATTACH_FILE:
                if (data != null && data.getData() != null) {

                    if ("file".equalsIgnoreCase(data.getData().getScheme())) {
                        onAttach();
                        getEmoticonInputPanel().hideLowerPanelIfVisible();
                        attachFileTransfer(data.getData().getPath());
                    } else {

                        //This won't work if the app doesn't have read access to the file
                        //Cursor cursor = getContentResolver().query(data.getData(), new String[]{MediaStore.MediaColumns.DATA}, null, null, null);
                        Cursor cursor = getContentResolver().query(data.getData(), null, null, null, null);
                        if (cursor.moveToFirst()) {
                            String displayName = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME));

                            for (int i=0;i<cursor.getColumnCount();++i) {
                                String name = cursor.getColumnName(i);
                                String val = cursor.getString(i);
                                Logger.d("onActivityResult: col["+i+"]: "+name+"="+val);
                            }

                            Logger.d("onActivityResult: displayName="+displayName+" cur="+cursor);


                            InputStream inputStream = null;
                            OutputStream out = null;
                            try {

                                inputStream = getContentResolver().openInputStream(data.getData());

                                String prefix;
                                String suffix = null;
                                if (TextUtils.isEmpty(displayName)) {
                                    prefix = String.valueOf(System.currentTimeMillis());
                                } else {
                                    int sep = displayName.indexOf('.');
                                    if (sep < 0) {
                                        prefix = displayName;
                                    } else {
                                        prefix = displayName.substring(0, sep);
                                        suffix = displayName.substring(sep, displayName.length());
                                    }
                                }

                                Logger.d("onActivityResult: prefix="+prefix+" suffix="+suffix);

                                File tmp = File.createTempFile(prefix, suffix);
                                out = new BufferedOutputStream(new FileOutputStream(tmp));
                                IOUtils.copy(inputStream, out);


                                Logger.d("onActivityResult: tmp.exists="+tmp.exists()+" tmp.length() ="+tmp.length() );

                                if (tmp.exists() && tmp.length() > 0) {
                                    onAttach();
                                    getEmoticonInputPanel().hideLowerPanelIfVisible();
                                    attachFileTransfer(tmp.getAbsolutePath());
                                }
                            } catch (IOException e) {
                                Logger.w(e, "Failed to open "+data);
                                RichChatUtils.showDefaultToast(this, "Failed to open file");
                            } finally {
                                IOHelper.safeClose(inputStream);
                                IOHelper.safeClose(out);
                            }
                        }
                    }
                    break;

                }
        }
    }


    protected void onAttach() {
        if (mIsAttachmentPreremoved) {
            removeAttachment();
        }
    }

    /**
     * Removes the attachment
     */
    protected void removeAttachment() {
        mAttachFileTransferFilePath = "";

        mContextContentType = null;
        mContextContentId = null;
        mContextUserUri = null;

        mIsAttachmentPreremoved = false;

        showAttachmentPane(false);
    }


    protected void attachFileTransfer(final String filePath) {
        final File file = (filePath != null) ? new File(filePath) : null;

        if (file == null || !file.exists()) {
            if (file != null) {
                //sometimes file permissions will cause file.exists() to return false
                //don't log file name since it could be private info, just log dir
                Logger.i("attachFileTransfer: file doesn't exist in parent dir "+file.getParent());
            }
            return;
        }

        getEmoticonInputPanel().removeAttachementView();
        removeAttachment();
        mAttachmentView = new AttachmentView(this);
        mAttachmentView.setCancelButtonOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                removeAttachment();
            }
        });

        mAttachFileTransferFilePath = filePath;

        final int fileResource = FileUtil.getFileResourceForFileType(filePath);
       if (FileUtil.isImage(filePath)) {
           mAttachmentView.getThumbnail().setImageResource(fileResource);
            mAttachmentView.getThumbnail().setImageURI(Uri.fromFile(file));
        } else {
            mAttachmentView.getThumbnail().setImageResource(fileResource);
        }
        mAttachmentView.setPrimaryText(file.getName());
        mAttachmentView.setSecondaryText(FileUtil.getConvertedFilesize(this, file.length()));
        showAttachmentPane(true);

        jumpToBottomMessage();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getEmoticonInputPanel() != null) {
                    getEmoticonInputPanel().setLowerPanel(EmoticonInputPanel.Mode.Keyboard);
                }
            }
        }, 200);
    }

    protected String getAttachedFileTransferPath() {
        return mAttachFileTransferFilePath;
    }

    protected void setAttachedFileTransferPath(String path) {
        mAttachFileTransferFilePath = path;
    }

    protected void sendFileTransfer(final String text, final String filePath) {
        final File file = new File(filePath);
        final String suggestedFileName = file.getName();

        final MessageHelper.MessagePriorityLevel priorityLevel = getEmoticonInputPanel().getPriority();

        if (FileUtil.isImage(filePath)) {
            //Picture transfer
            //Create a thumb file
            AsyncTask createThumbnail = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    String thumbPath;
                    ChatMessageSend.ThumbPolicy thumbPolicy = ChatMessageSend.ThumbPolicy.Move;
                    try {
                        if (file.length() > ImageUtil.MAX_HOSTED_THUMB_FILE_SIZE) {
                            thumbPath = ImageUtil.getUserIconTempPath(RichChatActivity.this, false);
                            ImageUtil.compressJpegImage(filePath, thumbPath, ImageUtil.MAX_HOSTED_THUMB_FILE_SIZE, ImageUtil.MAX_HOSTED_THUMBNAIL_IMAGE_DIMENSION, RichChatActivity.this);
                        } else {
                            //The file being sent is small enough to fit as the thumbnail, no need to resize
                            thumbPath = filePath;
                            thumbPolicy = ChatMessageSend.ThumbPolicy.Copy;
                        }
                    } catch (IOException ioe) {
                        Logger.w(ioe, "Failed to create thumbnail");
                        thumbPath = null;
                    }


                    JSONObject data = MessageHelper.setPriority(null, priorityLevel);
                    data = MessageHelper.setSuggestedFileName(data, suggestedFileName);

                    ChatMessageSend messageSend = new ChatMessageSend(mChatId,
                            MessageHelper.PHOTO_TAG)
                            .content(text)
                            .data(data)
                            .filePolicy(ChatMessageSend.FilePolicy.Copy)
                            .file(filePath)
                            .thumb(thumbPath)
                            .thumbPolicy(thumbPolicy);

                    BBMEnterprise.getInstance().getBbmdsProtocol().send(messageSend);
                    return null;
                }
            };
            createThumbnail.execute();
        } else {
            //Generic file
            JSONObject data = MessageHelper.setPriority(null, priorityLevel);
            data = MessageHelper.setSuggestedFileName(data, suggestedFileName);

            ChatMessageSend messageSend = new ChatMessageSend(mChatId,
                    MessageHelper.FILE_TAG)
                    .content(text)
                    .data(data)
                    .filePolicy(ChatMessageSend.FilePolicy.Delete) //We make copies of generic files so let BBM core delete it
                    .file(filePath);

            BBMEnterprise.getInstance().getBbmdsProtocol().send(messageSend);
        }
    }



    private boolean attachFromGallery() {
        if (PermissionsUtil.checkOrPromptSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            PermissionsUtil.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_PICTURES, R.string.rationale_write_external_storage)) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");


            startActivityForResult(intent, REQUEST_CODE_ATTACH_PICTURE);
            return true;
        }
        return false;
    }



    private void attachFromFilePicker() {
        if (PermissionsUtil.checkOrPromptSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                PermissionsUtil.PERMISSION_WRITE_EXTERNAL_STORAGE_REQUEST_TO_ATTACH_FILES, R.string.rationale_write_external_storage)) {
            final Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            final Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            try {
                startActivityForResult(intent, REQUEST_CODE_ATTACH_FILE);
            } catch (ActivityNotFoundException e) {
                //App should probably have their own custom built in file browser instead of relying on one being installed
                RichChatUtils.showDefaultToast(RichChatActivity.this, "File manager app not found");
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bbm_invitemore:
                Logger.gesture("InviteMore onItemClick", RichChatActivity.class);
                startInviteMoreActivity();
                return true;
            case R.id.conversation_menu_delete:
                deleteChat(false);
                return true;

            case R.id.menu_retract_chat:
                deleteChat(true);
                return true;

            case R.id.bbm_start_call:
                Logger.gesture("StartCall onItemClick", RichChatActivity.class);
                startCall();
                return true;

            case R.id.menu_refresh_keys:
                Logger.gesture("Refresh Protected Keys", RichChatActivity.class);
                ProtectedManager.getInstance().resyncAllUserKeys();

        }

        return super.onOptionsItemSelected(item);
    }

    private void startCall() {
        //create a local observable list of users to select from, if we add later when have user info the dialog should update
        final ArrayObservableList<AppUser> contacts = new ArrayObservableList();
        for (Participant participant : mParticipants.get()) {
            final ObservableValue<User> user = BBMEnterprise.getInstance().getBbmdsProtocol().getUser(participant.userUri);

            if (user.get().getExists() == Existence.YES) {
                final ObservableValue<AppUser> contact = UserManager.getInstance().getUser(user.get().regId);
                if (contact.get().getExists() == Existence.YES) {
                    contacts.add(contact.get());
                }
            } else if (user.get().getExists() == Existence.MAYBE) {
                mObserveConnector.connect(user, new Observer() {
                    @Override
                    public void changed() {
                        if (user.get().getExists() == Existence.YES) {
                            ObservableValue<AppUser> contact = UserManager.getInstance().getUser(user.get().regId);
                            if (contact.get().getExists() == Existence.YES) {
                                contacts.add(contact.get());
                            }
                        }
                    }
                });
            }
        }

        Logger.d("Found "+contacts.size()+" contacts for "+mParticipants.get().size()+" mParticipants");
        if (contacts.size() == 0) {
            //didn't find any, just prompt with all contacts
            RichChatUtils.promptToStartCall(this);
        } else if (contacts.size() == 1) {
            //just call this 1 user
            MediaUtils.makeCall(RichChatActivity.this, contacts.get(0).getRegId());
        } else {
            RichChatUtils.promptToStartCall(this, contacts);
        }
    }

    private void deleteChat(boolean shred) {
        //remove any draft message for this chat
        BbmUtils.saveDraftMessage(mChatId, "");

        if (shred) {
            BBMEnterprise.getInstance().getBbmdsProtocol().send(new ChatShred(mChatId));
        } else {
            //this will remove it from other device also
            BBMEnterprise.getInstance().getBbmdsProtocol().send(new StopConversation(Collections.singletonList(new StopConversation.Conversations(StopConversation.Conversations.Action.Leave, BbmUtils.chatIdToConversationUri(mChatId)))));
        }

        finish();
    }

    private void startInviteMoreActivity() {

        //calling get on a ObservableList can be expensive since it normally makes a copy, so just call once here
        List<Participant> participantList = mParticipants.get();
        final HashSet<Long> currentOtherRegIds = new HashSet<>(participantList.size());
        for (int i=0;i<participantList.size();++i) {
            currentOtherRegIds.add(BBMEnterprise.getInstance().getBbmdsProtocol().getUser(participantList.get(i).userUri).get().regId);
        }

        ChooseAppUserDialog.promptToSelectMultiple(RichChatActivity.this,
                null, //no title needed
                getString(R.string.start_chat), //the ok button text
                null, //don't allow to change subject
                new AppUserSelectedCallback() {
                    @Override
                    public void selected(Collection<AppUser> appUsers, String extraText) {
                        RichChatUtils.createNewChat(RichChatActivity.this, appUsers, extraText);
                    }

                    @Override
                    public void selected(AppUser appUser) {
                        //won't be called in multiselect mode
                    }
                },
                //pass a filter to preselect the users already in this chat, gives user ability to add or remove
                new AppUserFilter() {
                    @Override
                    public boolean matches(AppUser appUser) {
                        //just check if it was in the set of existing MPC participants
                        final long regId = appUser.getRegId();
                        return regId != 0 && currentOtherRegIds.contains(regId);
                    }
                }
        );
    }


    private boolean isLocalUserTyping() {
        return mTypingNotificationHelper != null
                && mTypingNotificationHelper.getLastTimeTypingNotification() != -1
                && (mTypingNotificationHelper.getLastTimeTypingNotification() + TYPING_NOTIFICATION_DELAY) > System.currentTimeMillis();
    }
}
