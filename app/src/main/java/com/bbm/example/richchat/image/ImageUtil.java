/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat.image;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bbm.example.richchat.R;
import com.bbm.example.common.ui.messages.FileUtil;
import com.bbm.example.richchat.util.RichChatUtils;
import com.bbm.example.richchat.util.StringUtil;
import com.bbm.sdk.common.IOHelper;
import com.bbm.sdk.support.util.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ImageUtil {
    public final static int MAX_HOSTED_THUMBNAIL_IMAGE_DIMENSION = 500;
    public final static int MAX_IMAGE_FILE_SIZE = 32 * 1024; // 32KB
    public final static int MAX_HOSTED_THUMB_FILE_SIZE = 52 * 1024; // 32KB

    public static final String MIME_TYPE_JPEG = "image/jpeg";
    public static final String MIME_TYPE_GIF = "image/gif";
    private static final String MIME_TYPE_TIFF = "image/tiff";

    private static final int ORIENTATION_NORMAL = 0;
    public static final int ORIENTATION_ROTATE_90 = 90;
    private static final int ORIENTATION_ROTATE_180 = 180;
    public static final int ORIENTATION_ROTATE_270 = 270;

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /**
     * Should avoid using this since it can result in using more than double the file size as 90 quality
     * with very little if any visible difference
     */
    private static final int DEFAULT_IMAGE_QUALITY = 95;

    public static String getUserIconTempPath(final Context context, final boolean isAnimated) throws IOException {
        final String extension = isAnimated ? ".gif" : ".jpg";
        File file = File.createTempFile(String.valueOf(System.currentTimeMillis()), extension);
        return file.getAbsolutePath();
    }

    /**
     * Save the given picturePath to the gallery under the BBM album / folder
     *
     * @param picturePath The path of the picture to save
     * @param context The activity context
     * @param mimeType Optional mime type
     */
    public static void saveImageToGallery(final String picturePath, final Context context, final String mimeType) {
        if (picturePath == null || context == null) {
            return;
        }
        final Uri fileUri = getOutputMediaFileUri(ImageUtil.MEDIA_TYPE_IMAGE, mimeType);
        try {
            FileUtil.copyFile(picturePath, fileUri.getPath());
        } catch (final IOException e) {
            RichChatUtils.showDefaultToast(context, context.getString(R.string.save_picture_to_gallery_generic_error));
            Logger.e(e);
            return;
        }
        scanMedia(context, fileUri);
        RichChatUtils.showDefaultToast(context, context.getString(R.string.save_picture_to_gallery_success));
    }

    /**
     * Invokes the media scanner to scan the uri for updating gallery..
     */
    public static void scanMedia(final Context context, final Uri contentUri) {
        final Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    /*
     * Compress image file for profile icon use. The Bitmap data will written to filePath and
     * image cropped square to MAX_IMAGE_DIMENSION with a resulting file size less than maxFileSize.
     */
    public static boolean compressJpegImage(final Bitmap bitmap, final String filePath, final boolean addSquareCrop, final long maxFileSize)
            throws IOException {
        return compressJpegImage(bitmap, filePath, addSquareCrop, maxFileSize, DEFAULT_IMAGE_QUALITY);
    }

    /*
     * Compress image file for profile icon use. The Bitmap data will written to filePath and
     * image cropped square to MAX_IMAGE_DIMENSION with a resulting file size less than maxFileSize.
     */
    private static boolean compressJpegImage(final Bitmap bitmap, final String filePath, final boolean addSquareCrop, final long maxFileSize, final int startingImageQuality)
            throws IOException {
        boolean result = true;
        if (bitmap == null) {
            return false;
        }
        Bitmap croppedBitmap;
        if (addSquareCrop) {
            croppedBitmap = ImageUtil.cropSquare(bitmap);
        } else {
            croppedBitmap = bitmap;
        }

        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();

        FileOutputStream out = null;
        try{
            out = new FileOutputStream(file);
            croppedBitmap.compress(Bitmap.CompressFormat.JPEG, startingImageQuality, out);
            out.close();

            boolean canScaleDownImageSize = true;
            long len = file.length();
            int resizeCount = 1;
            Logger.d("ImageUtil.compressJpegImage: initial compress len=" + file.length() + " W=" + croppedBitmap.getWidth() + " H=" + croppedBitmap.getHeight() + " maxFileSize=" + maxFileSize + " startingImageQuality=" + startingImageQuality);

            Bitmap croppedAndScaledBitmap = croppedBitmap;
            while (len > maxFileSize && canScaleDownImageSize) {
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
                out = new FileOutputStream(file);

                // First run compress image to size of image loaded in memory.
                // subsequent loops lower quality until image size is less then max file size.
                int jpegQuality = startingImageQuality - 10;
                while (jpegQuality >= 50 && croppedAndScaledBitmap.compress(Bitmap.CompressFormat.JPEG, jpegQuality, out)) {

                    out.flush();
                    out.close();
                    file = new File(filePath);
                    Logger.d("ImageUtil.compressJpegImage: jpegQuality="+jpegQuality+" len="+file.length()+" width="+croppedAndScaledBitmap.getWidth()+" height="+croppedAndScaledBitmap.getHeight()+" resizeCount="+resizeCount+" maxFileSize="+maxFileSize);

                    if (file.length() < maxFileSize) {
                        return result;
                    }
                    if (file.exists()) {
                        file.delete();
                    }
                    file.createNewFile();
                    out = new FileOutputStream(file);
                    jpegQuality -= 10;
                }
                out.flush();
                out.close();
                //if compressing quality failed, we should reduce the size of dimensions.
                final int width = croppedBitmap.getWidth() / (2 * resizeCount);
                final int height = croppedBitmap.getHeight() / (2 * resizeCount);
                if (width <= 0 || height <= 0) {
                    Logger.w("Cannot scale down image size further width "+width+", height "+ height);
                    canScaleDownImageSize = false;
                    result = false;
                } else {
                    try {
                        Logger.d("ImageUtil.compressJpegImage: compress with reduced image size to width "+width+", height "+ height);

                        // Use a smaller image in next iteration of compression algorithm.
                        croppedAndScaledBitmap = Bitmap.createScaledBitmap(croppedBitmap, width, height, false);

                        file = new File(filePath);
                        if (file.exists()) {
                            file.delete();
                        }
                        file.createNewFile();
                        out = new FileOutputStream(file);
                        croppedAndScaledBitmap.compress(Bitmap.CompressFormat.JPEG, DEFAULT_IMAGE_QUALITY, out);
                        out.flush();
                        out.close();
                        len = file.length();
                        resizeCount++;
                    } catch (final OutOfMemoryError e) {
                        Logger.e(e, "compress out of memory");
                        //return the worst quality
                        file = new File(filePath);
                        if (file.exists()) {
                            file.delete();
                        }

                        try {
                            file.createNewFile();
                            out = new FileOutputStream(file);
                            croppedBitmap.compress(Bitmap.CompressFormat.JPEG, 0, out);
                            out.flush();
                            if (file.length() < maxFileSize) {
                                return true;
                            }
                        } catch (final OutOfMemoryError er) {
                            Logger.e(er, "compress out of memory");
                            return false;
                        } finally {
                            if(out != null) {
                                try {
                                    out.close();
                                    out = null;
                                } catch (final IOException ioe) {
                                    Logger.e(ioe, "Could not close file");
                                }
                            }
                        }
                    }
                }
            }
        } catch (final Exception e ) {
            Logger.e(e, "Could not compress image");
        } finally {
            if(out != null) {
                try {
                    out.close();
                } catch (final IOException ioe) {
                    Logger.e(ioe, "Could not close file");
                }
            }
        }
        return result;
    }

    /**
     * Compress image file for profile use. Image will be loaded from filePath and overwritten with an
     * image cropped to MAX_IMAGE_DIMENSION and file size less than MAX_IMAGE_FILE_SIZE.
     */
    public static boolean compressJpegImage(String srcFilePath, String dstFilePath, int maxSize, int maxDimension, Context context) {
        boolean success = false;
        try {
            // Load an appropriate sized photo in memory.
            final Bitmap photo = getOrientedBitmap(srcFilePath, new Point(maxDimension,
                    maxDimension), context);
            success = compressJpegImage(photo, dstFilePath, false, MAX_IMAGE_FILE_SIZE);
        } catch (final IOException | NullPointerException e) {
            Logger.e(e);
        }
        return success;
    }

    /**
     * Get an instance of a Bitmap from an absolute filepath oriented correctly, if EXIF
     * orientation metadata exists.
     */
    public static Bitmap getOrientedBitmap(final String filename, final Point size, Context context)
            throws IOException {
        return getOrientedBitmap(filename, size, true, ImageView.ScaleType.CENTER_CROP, context);
    }

    /**
     * Get an instance of a Bitmap from an absolute filepath oriented correctly, if EXIF
     * orientation metadata exists.
     */
    public static Bitmap getOrientedBitmap(final String filename, final Point size, final boolean useInBitmap, ImageView.ScaleType scaleType, Context context)
            throws IOException {
        final Bitmap sourceBitmap = decodeSampledBitmapFromFile(filename, size.x, size.y, useInBitmap, false, scaleType, context);
        final Matrix matrix = getBitmapOrientationMatrixFromEXIFTag(filename);
        // if matrix does not equal the identity matrix continue with applying a scale to the matrix.
        if (!matrix.isIdentity() && sourceBitmap != null) {
            return com.cropimage.Util.rotateImage(sourceBitmap, size, getOrientation(filename));
        }
        return sourceBitmap;
    }

    /**
     * Decode and sample down a bitmap from a file to the requested width and height.
     *
     * @param filename     The full path of the file to decode
     * @param reqWidth     The requested width of the resulting bitmap
     * @param reqHeight    The requested height of the resulting bitmap
     * @param cache        The ImageCache used to find candidate bitmaps for use with inBitmap
     * @param useInBitmap  Set true to allow decoding into existing unused bitmaps in the ImageCache
     * @param useThumbnail Set true to allow use of an exif attached thumbnail instead of decoding the original file
     * @param scaleType    The scaleType used to find scale ratio, CENTER_CROP will get a bitmap with both dimensions equal or exceeded, otherwise both dimensions will be equal or less
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     * that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromFile(final String filename, final int reqWidth,
                                                     final int reqHeight, boolean useInBitmap, boolean useThumbnail, ImageView.ScaleType scaleType, Context context) {
        int preferredSampleSize = 1;
        int sampleSize = 1;
        Bitmap b = null;
        InputStream is = null;
        BitmapFactory.Options options = new BitmapFactory.Options();

        // Quick check, ensure the file name is valid. asdfs
        if (filename == null || StringUtil.isEmpty(filename)) {
            return b;
        }

        // Get the preferred bytes to be used, if it comes back, we want to work
        // with this set of data.
        byte[] preferred = useThumbnail ? getPreferredImageBytes(filename) : null;
        boolean hasPreferred = (preferred != null && preferred.length > 0);

        // Need to get the file info first.
        options.inJustDecodeBounds = true;

        try {
            if (hasPreferred) {
                BitmapFactory.decodeByteArray(preferred, 0, preferred.length, options);
            } else {
                try {
                    is = new FileInputStream(filename);
                    // Need to decode the options to determine if the file mimetype
                    BitmapFactory.decodeStream(is, null, options);
                } catch (Exception e) {
                    Logger.d(e, "Failed to fetch bitmap");
                } finally {
                    // Clean up the input stream
                    IOHelper.safeClose(is);
                }
            }
        } catch (OutOfMemoryError error) {
            Logger.i(error, "ImageFetcher failed out of memory");
            return b;
        }

        // Reset the flag
        options.inJustDecodeBounds = false;

        // Determine the sample size to be used.
        if (hasPreferred) {
            preferredSampleSize = calculateSampleSizeLocked(options.outWidth, options.outHeight,
                    reqWidth, reqHeight, scaleType, context);
        }

        // Still calculate the sample size here, as a back up to the preferred sample size.
        sampleSize = calculateSampleSizeLocked(options.outWidth, options.outHeight, reqWidth, reqHeight, scaleType, context);

        try {
            if (hasPreferred) {
                // If we are using the preferred image, try to decode the image as efficient as
                // possible. If we can use the inbitmap to reuse memory and release asap.
                options.inSampleSize = preferredSampleSize;

                if (useInBitmap && !options.outMimeType.equals(ImageUtil.MIME_TYPE_GIF)) {
                    ImageUtil.addInBitmapOptions(options);
                }

                b = BitmapFactory.decodeByteArray(preferred, 0, preferred.length, options);

                // If we did use inBitmap, release the memory of that bitmap.
                if (options.inBitmap != null && b != options.inBitmap) {
                    options.inBitmap.recycle();
                    options.inBitmap = null;
                }
            }

            if (b == null) {

                // If we hit here, the preferred image was not available or not usable. But we
                // can try doing the same operation on the original image.
                is = new FileInputStream(filename);
                options.inSampleSize = sampleSize;

                if (useInBitmap && !options.outMimeType.equals(ImageUtil.MIME_TYPE_GIF)) {
                    ImageUtil.addInBitmapOptions(options);
                }

                b = BitmapFactory.decodeStream(is, null, options);

                // If we did use inBitmap, release the memory of that bitmap.
                if (options.inBitmap != null && b != options.inBitmap) {
                    options.inBitmap.recycle();
                    options.inBitmap = null;
                }
            }
        } catch (Exception e) {
            Logger.d(e, "Failed to fetch bitmap");
            b = null;
        } catch (OutOfMemoryError error) {
            Logger.i(error, "Unable to fetch bitmap");
            b = null;
        } finally {
            // Clean up the input stream
            IOHelper.safeClose(is);
        }

        return b;
    }

    /**
     * Given a path to the image, determine if any exif data exists, in this case
     * we want the embedded thumbnail data, it will be much smaller from the overall
     * bitmap, thus saving memory
     *
     * @param filename The path to the image file to be used.
     * @return null if no thumbnail image is embedded.
     */
    private static byte[] getPreferredImageBytes(String filename) {
        byte[] thumbnail = null;
        try {
            ExifInterface exif = new ExifInterface(filename);
            if (exif.hasThumbnail()) {
                thumbnail = exif.getThumbnail();
            }
        } catch (IOException e) {
        }
        return thumbnail;
    }

    /**
     * Calculation to determine the Sample Size based on the orignal width and height and
     * desired width and height.
     *
     * @param width     The original image width
     * @param height    The original image height
     * @param dwidth    The desired image width
     * @param dheight   The desired image height
     * @param scaleType The scaleType used to find scale ratio, CENTER_CROP will get a bitmap with both dimensions equal or exceeded, otherwise both dimensions will be equal or less
     * @return The calculated sample size to be used, default is 1.
     */
    private static int calculateSampleSizeLocked(int width, int height, int dwidth, int dheight, ImageView.ScaleType scaleType, Context context) {
        final int maxDimension = ImageUtil.getMaxBitmapHeightOpenGL(context);
        if (dwidth <= 0 || dwidth > maxDimension) {
            dwidth = maxDimension;
        }
        if (dheight <= 0 || dheight > maxDimension) {
            dheight = maxDimension;
        }
        float scale;
        if (scaleType == ImageView.ScaleType.CENTER_CROP) {
            // center crop, scale according the dimension with smaller factor
            if (width * dheight > dwidth * height) {
                scale = (float) height / (float) dheight;
            } else {
                scale = (float) width / (float) dwidth;
            }
        } else {
            // no crop, scale according the dimension with larger factor
            if (width * dheight < dwidth * height) {
                scale = (float) height / (float) dheight;
            } else {
                scale = (float) width / (float) dwidth;
            }
        }
        int result = Math.round(scale);
        result = result > 0 ? result : 1;
        // While we calculating the scale for downscaling, we also need to check if the scale is suitable for target dimension
        // Otherwise will cause OpenGLRenderer issues like BBM-34278, BBM-34332
        if ( width / result > maxDimension || height/ result > maxDimension )  {
            // If the scale will result in over size we should use the greater length for the scale calculate
            scale = ( width > height ? (float)width / (float)dwidth : (float)height / (float)dheight );
            result = Math.round(scale);
            result = result > 0 ? result : 1;
        }
        return result;
    }

    /**
     * Reads the exif information from a bitmap file such as a jpeg to create
     * a matrix with a rotation good for ImageView.setImageMatrix();
     *
     * @param filepath The full path to the image file.
     * @return Matrix containing the rotation data
     */
    private static Matrix getBitmapOrientationMatrixFromEXIFTag(final String filepath) {
        final Matrix matrix = new Matrix();
        final int orientation = getOrientation(filepath);
        switch (orientation) {
            case ORIENTATION_ROTATE_90:
                matrix.postRotate(ORIENTATION_ROTATE_90);
                break;
            case ORIENTATION_ROTATE_180:
                matrix.postRotate(ORIENTATION_ROTATE_180);
                break;
            case ORIENTATION_ROTATE_270:
                matrix.postRotate(ORIENTATION_ROTATE_270);
                break;
            default:
                break;
        }
        return matrix;
    }

    /**
     * Get the orientation of a jpeg file by reading the Exif information.
     *
     * @param filename jpeg image
     * @return orientation 0,90,180,270 or -1 in case of error.
     */
    public static int getOrientation(final String filename) {
        int orientation = -1;
        try {
            final ExifInterface exif = new ExifInterface(StringUtil.safeString(filename));
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
        } catch (final IOException e) {
            Logger.e(e);
        }

        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                orientation = ORIENTATION_NORMAL;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                orientation = ORIENTATION_ROTATE_90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                orientation = ORIENTATION_ROTATE_180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                orientation = ORIENTATION_ROTATE_270;
                break;
            default:
                break;
        }
        return orientation;
    }


    /**
     * Create a file Uri for saving an image or video
     *
     * @param type     {@link #MEDIA_TYPE_IMAGE} or {@link #MEDIA_TYPE_VIDEO}
     * @param mimeType Optional mime type
     * @see {@link #getMimeType(String)} or {@link #getMimeType(Context, Uri)}
     */
    public static Uri getOutputMediaFileUri(final int type, final String mimeType) {
        final File file = getOutputMediaFile(type, mimeType);
        if (file != null) {
            return Uri.fromFile(file);
        } else {
            return null;
        }
    }

    private static final String OutputMediaDirectoryName = "RichChat";

    /**
     * Create a File for saving and sharing an image or video
     *
     * @param type     {@link #MEDIA_TYPE_IMAGE} or {@link #MEDIA_TYPE_VIDEO}
     * @param mimeType Optional mime type
     * @see {@link #getMimeType(String)} or {@link #getMimeType(Context, Uri)}
     */
    public static File getOutputMediaFile(final int type, final String mimeType) {
        // To be safe, check that external storage is mounted or emulated.
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            Logger.d("no external media available.");
            // Tested on Samsung Galaxy S2 without an SD card that this safe test will fail
            // but getOutputMediaFile still returns successfully.
            // return null;
        }

        final File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), OutputMediaDirectoryName);
        mediaStorageDir.mkdirs();

        return newFile(mediaStorageDir.getPath(), type, mimeType);
    }

    /**
     * @param type     {@link #MEDIA_TYPE_IMAGE} or {@link #MEDIA_TYPE_VIDEO}
     * @param mimeType Optional mime type
     * @see {@link #getMimeType(String)} or {@link #getMimeType(Context, Uri)}
     */
    private static File newFile(final String directory, final int type, final String mimeType) {
        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        File mediaFile = null;
        if (type == MEDIA_TYPE_IMAGE) {
            String extension;
            if (!TextUtils.isEmpty(mimeType)) {
                switch (mimeType) {
                    case MIME_TYPE_GIF:
                        extension = ".gif";
                        break;
                    case MIME_TYPE_TIFF:
                        extension = ".tiff";
                        break;
                    default:
                        extension = ".jpg";
                }
            } else {
                extension = ".jpg";
            }

            mediaFile = new File(directory + File.separator + "IMG_" + timeStamp + extension);
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(directory + File.separator + "VID_" + timeStamp + ".mp4");
        }
        return mediaFile;
    }

    @SuppressWarnings("SuspiciousNameCombination")
    public static Bitmap cropSquare(final Bitmap photo) {
        if (photo == null) {
            Logger.w("ImageUtils.cropSquare. Bitmap is null return");
            return null;
        }
        Bitmap croppedPhoto = photo;
        final int width = photo.getWidth();
        final int height = photo.getHeight();
        if (width > height) {
            final int diff = width - height;
            croppedPhoto = Bitmap.createBitmap(photo, diff / 2, 0, height, height);
            photo.recycle();
            Logger.v("ImageUtils.cropSquare. Cropped height");
        } else if (height > width) {
            final int diff = height - width;
            croppedPhoto = Bitmap.createBitmap(photo, 0, diff / 2, width, width);
            photo.recycle();
            Logger.v("ImageUtils.cropSquare. Cropped width");
        }
        return croppedPhoto;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static void addInBitmapOptions(final BitmapFactory.Options options) {
        // inBitmap only works with mutable bitmaps so force the decoder to
        // return mutable bitmaps.
        options.inMutable = true;
    }

    public static int getMaxBitmapHeightOpenGL(final Context context) {
        final Resources resources = context.getResources();
        final int screenHeight = resources.getDisplayMetrics().heightPixels;
        return Math.max(2048, screenHeight);
    }
}

