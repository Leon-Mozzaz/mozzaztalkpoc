/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.content.Context;
import android.text.TextUtils;

import com.bbm.sdk.support.util.BbmUtils;
import com.bbm.example.richchat.BBMNotificationManager.NotificationItem;

import com.bbm.sdk.bbmds.ChatMessage;
import com.bbm.sdk.service.ProtocolConnector;
import com.bbm.sdk.service.ProtocolMessage;
import com.bbm.sdk.service.ProtocolMessageConsumer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ChatNotificationModel implements ProtocolMessageConsumer, BBMNotificationManager.BBMNotificationProvider {

    private final Map<String, NotificationItem> mNotificationItemsMap;
    private final Map<String, NotificationItem> mPendingNotificationItemsMap;

    // This is used to enable/disable handling of notifications, by default it should be enabled.
    private boolean mAllowNotificationHandling = true;

    private String mIgnoredUri;
    private Context mContext;

    public ChatNotificationModel(Context context, final ProtocolConnector protocolConnector) {
        mContext = context;
        protocolConnector.addMessageConsumer(this);
        mNotificationItemsMap = new HashMap<>();
        mPendingNotificationItemsMap = new HashMap<>();
    }

    @Override
    public void onMessage(final ProtocolMessage message) {
        // Check the state, if disabled do nothing.
        if(!mAllowNotificationHandling) {
            return;
        }

        if (message.getType().equals("listAdd")) {
            final JSONObject data = message.getData();
            final String type = data.optString("type");
            if (type.equals("chatMessage")) {
                parseElementsForChatMessages(data.optJSONArray("elements"));
                // Message notification here
            }
        }
    }

    @Override
    public void resync() {
        // No need to re-sync since this only tracks notifications
    }

    @Override
    public Collection<NotificationItem> getNotifications() {
        return mNotificationItemsMap.values();
    }

    @Override
    public void clear() {
        // Clearing the map is not enough. Must dispose of the monitor too.
        for (NotificationItem item : mPendingNotificationItemsMap.values()){
            if (item != null && item instanceof ChatMessageNotificationItem) {
                ((ChatMessageNotificationItem) item).dispose();
            }
        }
        mNotificationItemsMap.clear();
    }

    @Override
    public void setIgnoreUri(final String uri) {
        mIgnoredUri = uri;
    }

    @Override
    public void allowNotificationHandling(boolean enable) {
        mAllowNotificationHandling = enable;
    }

    void addPending(final String key) {
        if (mPendingNotificationItemsMap.containsKey(key)) {
            mNotificationItemsMap.put(key, mPendingNotificationItemsMap.remove(key));
            RichChatApp.getNotificationManager().post();
        } else if (mNotificationItemsMap.containsKey(key)) {
            RichChatApp.getNotificationManager().post(true);
        }
    }

    public void removeNotification(final String key) {
        mPendingNotificationItemsMap.remove(key);
        mNotificationItemsMap.remove(key);
    }

    public boolean isNotificationIgnored (final String key) {
        if (mIgnoredUri == null || TextUtils.isEmpty(key)) {
            return false;
        }
        return key.equals(mIgnoredUri);
    }

    private void insertNotification(final String key, final NotificationItem value) {
        if (mIgnoredUri == null || !isNotificationIgnored(key)) {
            mPendingNotificationItemsMap.put(key, value);
            value.activate();
        }
    }

    private void parseElementsForChatMessages(final JSONArray elements) {
        for (int i = 0; i < elements.length(); i++) {
            final JSONObject element = elements.optJSONObject(i);
            setElementsForChatMessages(element);
        }
    }

    private void setElementsForChatMessages(final JSONObject element) {
        final ChatMessage chatMessage = new ChatMessage();
        chatMessage.setAttributes(element);

        if (chatMessage.hasFlag(ChatMessage.Flags.Incoming) && chatMessage.state != ChatMessage.State.Read) {
            final ChatMessageNotificationItem item = new ChatMessageNotificationItem(mContext, this, chatMessage);

            insertNotification(BbmUtils.chatIdToConversationUri(chatMessage.chatId), item);
        }
    }
}
