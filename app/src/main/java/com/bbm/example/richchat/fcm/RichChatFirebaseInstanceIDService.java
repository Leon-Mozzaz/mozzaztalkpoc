package com.bbm.example.richchat.fcm;

import com.bbm.sdk.support.util.Logger;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.service.BBMEnterpriseState;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class RichChatFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        Logger.d("onTokenRefresh:");
        if (BBMEnterprise.getInstance().getState().get() != BBMEnterpriseState.UNINITIALIZED) {
            BBMEnterprise.getInstance().setPushToken(FirebaseInstanceId.getInstance().getToken());
        }
    }
}