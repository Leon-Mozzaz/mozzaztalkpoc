/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat.media;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.bbm.example.richchat.R;
import com.bbm.example.common.util.PermissionsUtil;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.media.BBMECallCreationObserver;
import com.bbm.sdk.media.BBMEMediaManager;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MediaUtils {

    private static long mRegIdToCall;

    public static void makeCall(final Activity activity, Fragment fragment, long regId) {
        mRegIdToCall = regId;
        //Check for permission to access the microphone before starting an outgoing call
        if (PermissionsUtil.checkOrPromptSelfPermission(activity, fragment,
                Manifest.permission.RECORD_AUDIO,
                PermissionsUtil.PERMISSION_RECORD_AUDIO_FOR_VOICE_CALL,
                R.string.rationale_record_audio, PermissionsUtil.sEmptyOnCancelListener)) {

            //Ask the media service to start a call with the specified pin and include an observer to be notified of the result
            BBMEnterprise.getInstance().getMediaManager().startCall(regId, false, new BBMECallCreationObserver() {
                @Override
                public void onCallCreationSuccess(int callId) {
                    //The call was started successfully. Open our call activity
                    Intent inCallIntent = new Intent(activity, InCallActivity.class);
                    inCallIntent.putExtra(InCallActivity.EXTRA_CALL_ID, callId);
                    activity.startActivity(inCallIntent);
                }

                @Override
                public void onCallCreationFailure(BBMEMediaManager.Error error) {
                    //The call wasn't able to be started, provide an error to the user
                    Toast.makeText(activity, activity.getString(R.string.error_starting_call, error.name()), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public static void makeCall(final Activity activity, long regId) {
        //Check for permission to access the microphone before starting an outgoing call
        if (PermissionsUtil.checkOrPromptSelfPermission(activity,
                Manifest.permission.RECORD_AUDIO,
                PermissionsUtil.PERMISSION_RECORD_AUDIO_FOR_VOICE_CALL,
                R.string.rationale_record_audio)) {

            //Ask the media service to start a call with the specified user and include an observer to be notified of the result
            BBMEnterprise.getInstance().getMediaManager().startCall(regId, false, new BBMECallCreationObserver() {
                @Override
                public void onCallCreationSuccess(int callId) {
                    //The call was started successfully. Open our call activity
                    Intent inCallIntent = new Intent(activity, InCallActivity.class);
                    inCallIntent.putExtra(InCallActivity.EXTRA_CALL_ID, callId);
                    activity.startActivity(inCallIntent);
                }

                @Override
                public void onCallCreationFailure(BBMEMediaManager.Error error) {
                    //The call wasn't able to be started, provide an error to the user
                    Toast.makeText(activity, activity.getString(R.string.error_starting_call, error.name()), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public static void makeCallPermissionGranted(Activity activity, Fragment fragment) {
        if (mRegIdToCall != 0) {
            makeCall(activity, fragment, mRegIdToCall);
            mRegIdToCall = 0;
        }
    }

    /**
     * Utility method to convert a time in ms to an hours:minutes:seconds string to be displayed in the call activity
     */
    public static String getCallElapsedTimeFromMilliseconds(final long millis) {
        final long hours = TimeUnit.MILLISECONDS.toHours(millis);
        final long totalMinutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        final long minutes = totalMinutes - TimeUnit.HOURS.toMinutes(hours);
        final long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(totalMinutes);

        if (hours >= 1) {
            return String.format(Locale.getDefault(), "%d:%02d:%02d", hours, minutes, seconds);
        } else {
            return String.format(Locale.getDefault(), "%d:%02d", minutes, seconds);
        }
    }
}
