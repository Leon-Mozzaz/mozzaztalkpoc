/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.bbm.example.richchat.media.IncomingCallObserver;
import com.bbm.example.richchat.util.RichChatUtils;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.support.util.FirebaseHelper;
import com.bbm.sdk.support.util.Logger;

public class RichChatApp extends Application {
    private static Context sAppContext;

    private static BBMNotificationManager sNotificationManager;
    private static ChatNotificationModel sChatNotificationModel;

    public static BBMNotificationManager getNotificationManager() {
        return sNotificationManager;
    }

    public static Context getAppContext() {
        return sAppContext;
    }

    @Override
    public void onCreate() {

        Logger.setLogWriter(new Logger.LogWriter() {
            @Override
            public void log(int priority, Throwable t, String message, String tag) {
                //just write to logcat like default
                Log.println(priority, tag, Logger.formatMessage(t, message));
                if (Logger.USER_TAG.equals(tag)) {
                    //This is a message that could be displayed to a developer/tester user
                    //This is not recommended for production apps!
                    if (t != null) {
                        //this is even less user friendly, just for devs only recommended. Add exception type and message
                        message = message + "\n" + t;
                    }
                    RichChatUtils.showDefaultToast(getAppContext(), message);
                }
            }
        });

        super.onCreate();

        sAppContext = getApplicationContext();

        sNotificationManager = new BBMNotificationManager(this);
        sChatNotificationModel = new ChatNotificationModel(this, BBMEnterprise.getInstance().getBbmdsProtocolConnector());
        sNotificationManager.addProvider(sChatNotificationModel);

        //Register an observer for incoming voice and video calls.
        BBMEnterprise.getInstance().getMediaManager().
                addIncomingCallObserver(new IncomingCallObserver(getAppContext()));

        //sync our local user with firebase, retrieve all remote users, and start protected lite
        //pass true to also set the FCM push token
        FirebaseHelper.initUserDbSyncAndProtected(true);

        //Initialize BBMEnterprise SDK then start it
        BBMEnterprise.getInstance().initialize(this);
        BBMEnterprise.getInstance().start();

        Logger.d("RichChatApp.onCreate");
    }
}
