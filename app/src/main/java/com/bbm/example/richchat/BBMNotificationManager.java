/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.Html;

import com.bbm.example.richchat.util.IntentUtils;
import com.bbm.example.richchat.util.StringUtil;
import com.bbm.sdk.support.util.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public final class BBMNotificationManager {

    private static final int NOTIFICATION_ID = 2;
    private static final long[] VIBRATE_PATTERN = new long[]{0, 200, 50, 200, 50, 200};
    private static final int LIGHT_DURATION_ON = 300;
    private static final int LIGHT_DURATION_OFF = 1900;
    private static final int VOICE_ONGOING_NOTIFICATION_ID = 9000;


    private static final String ACTION_NOTIFICATION_DELETED = "com.bbm.enterprise.notification_deleted";

    private final NotificationManagerCompat mNotificationManager;
    private final List<BBMNotificationProvider> mProviders;

    private boolean mBeingCleared = false;

    private final Context mContext;

    private final BroadcastReceiver mDeletedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            Logger.i("Received broadcast about deleted notification: "+ intent);
            notificationDismissed();
        }
    };

    public BBMNotificationManager(final Context context) {
        mContext = context;
        mNotificationManager = NotificationManagerCompat.from(context);

        mProviders = new ArrayList<>();
        context.registerReceiver(mDeletedReceiver, new IntentFilter(ACTION_NOTIFICATION_DELETED));
    }

    /**
     * Helper to clear all the providers out of the notification manager. Usually only
     * called if application is shutting down.
     */
    public void removeAllProviders() {
        mProviders.clear();
    }

    public void addProvider(final BBMNotificationProvider provider) {
        mProviders.add(provider);
    }


    private NotificationCompat.Builder getBuilder() {
        final int ledNotificationState = 0;

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext)
                .setAutoCancel(true)
                .setDeleteIntent(PendingIntent.getBroadcast(mContext, 0, new Intent(ACTION_NOTIFICATION_DELETED), 0))
                //normal messages are considered high or default priority depending on whether the headsup is required
                //priority BBM msgs may need to override the priority with PRIORITY_MAX (or HIGH as appropriate)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE);


        // Set sound or nothing if silent
        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        // toggle vibrate
        builder.setVibrate(VIBRATE_PATTERN);

        setLedState(builder);

        return builder;
    }

    private void setLedState(NotificationCompat.Builder builder) {
        builder.setLights(0xffff0000, LIGHT_DURATION_ON, LIGHT_DURATION_OFF);
    }

    /**
     * Set the URI of the content displayed by the foreground activity so notifications
     * under that URI are supressed.
     *
     * @param uri of the current foreground activity
     */
    public void setIgnoreUri(final String uri) {
        for (final BBMNotificationProvider provider : mProviders) {
            provider.setIgnoreUri(uri);
        }
    }

    private int getLargeNotificationIconResourceId() {
        return R.drawable.bbm_notifications_v1;
    }

    private int getSmallNotificationIconResourceId() {
        return R.drawable.multiple_notifications;
    }

    public void post() {
        post(false);
    }

    public void post(boolean silent) {
        if (mBeingCleared) {
            return;
        }

        final TreeSet<BBMNotificationManager.NotificationItem> notificationMessages = new TreeSet<>(new NotificationComparator());
        for (final BBMNotificationProvider provider : mProviders) {
            notificationMessages.addAll(provider.getNotifications());
        }

        if (notificationMessages.size() == 0) {
            mNotificationManager.cancel(NOTIFICATION_ID);
            return;
        } else if (notificationMessages.size() > 1) {
            // Stacked notification
            final NotificationCompat.Builder stackBuilder = getBuilder()
                    .setSmallIcon(getSmallNotificationIconResourceId())
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), getLargeNotificationIconResourceId()))
                    .setContentTitle(mContext.getResources().getString(R.string.app_name));

            final NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle(stackBuilder);

            String ticker = "";
            int type = 0;
            long processedTimestamp = 0;
            for (final NotificationItem item : notificationMessages) {
                if (processedTimestamp > item.getTimestampInMs()){
                    continue;
                }else{
                    processedTimestamp = item.getTimestampInMs();
                }
                ticker = String.format(mContext.getString(R.string.notification_message_ticker_format),
                        item.getTitle(),
                        item.getMessage());

                style.addLine(Html.fromHtml(StringUtil.makeSingleLine(String.format(mContext.getString(R.string.stacked_notification_format),
                        item.getTitle(),
                        item.getMessage()))));

                // A stacked notification can only have 1 style, so if it's stacked the newest one wins
                if (item.getCustomProfile() != null) { // Handle custom way of notifying here i.e. a PING
                    final NotificationItem.Profile profile = item.getCustomProfile();
                    stackBuilder.setVibrate(profile.isVibrate() ? VIBRATE_PATTERN : new long[0]);

                    stackBuilder.setPriority(profile.getPriority());
                    setLedState(stackBuilder);
                }
                type |= item.getType();
            }
            stackBuilder.setContentIntent(getPendingIntentFor(type));
            stackBuilder.setTicker(ticker);
            final NotificationItem lastItem = notificationMessages.last();
            if(lastItem != null) {
                final String smallmessage = String.format(mContext.getString(R.string.notification_message_ticker_format),
                        notificationMessages.last().getTitle(),
                        notificationMessages.last().getMessage());

                stackBuilder.setContentText(smallmessage);
            }
            stackBuilder.setVisibility(NotificationCompat.VISIBILITY_PRIVATE);

            if (silent) {
                stackBuilder.setVibrate(null);
                stackBuilder.setSound(Uri.EMPTY);
            }

            mNotificationManager.notify(NOTIFICATION_ID, stackBuilder.build());
        } else {
            // single Notification
            final NotificationItem item = notificationMessages.first();
            final String ticker = String.format(mContext.getString(R.string.notification_message_ticker_format),
                    item.getTitle(),
                    item.getMessage());

            final Bitmap avatar = null;
                    PendingIntent pIntent = item.getIntent();
            NotificationCompat.Builder singleBuilder = getBuilder()
                    .setSmallIcon(item.getCustomIconResId() == -1 ? getSmallNotificationIconResourceId() : item.getCustomIconResId())
                    .setLargeIcon(avatar == null ? BitmapFactory.decodeResource(mContext.getResources(), getLargeNotificationIconResourceId()) : avatar)
                    .setContentTitle(item.getTitle())
                    .setContentText(item.getMessage())
                    .setTicker(ticker)
                    .setContentIntent(pIntent);

            if (item.getCustomProfile() != null) { // Handle custom way of notifying here i.e. a PING
                final NotificationItem.Profile profile = item.getCustomProfile();

                singleBuilder.setVibrate(profile.isVibrate() ? VIBRATE_PATTERN : new long[0]);
                singleBuilder.setPriority(profile.getPriority());

                if (profile.getSoundUri() != null) {
                    singleBuilder.setSound(Uri.parse(profile.getSoundUri()));
                }
                setLedState(singleBuilder);
            }

            if (silent) {
                singleBuilder.setVibrate(null);
                singleBuilder.setSound(Uri.EMPTY);
            }

            singleBuilder.setVisibility(NotificationCompat.VISIBILITY_PRIVATE);

            mNotificationManager.notify(NOTIFICATION_ID, singleBuilder.build());
        }
    }

    private PendingIntent getPendingIntentFor(final int type) {
        PendingIntent intent = IntentUtils.buildPendingIntentForChatsTab(mContext);

        return intent;
    }

    public void dismiss() {
        mNotificationManager.cancel(NOTIFICATION_ID);
        notificationDismissed();
    }

    private void notificationDismissed() {
        mBeingCleared = true;
        for (final BBMNotificationProvider provider : mProviders) {
            provider.clear();
        }
        mBeingCleared = false;
    }

    public void cancelOngoingNotification() {
        mNotificationManager.cancel(BBMNotificationManager.VOICE_ONGOING_NOTIFICATION_ID);
        dismiss();
    }

    public interface NotificationItem {
        int TYPE_MESSAGE = 4;

        interface Profile {
            String getSoundUri();

            boolean isVibrate();

            int ledColor();
            /**
             * this method returns the appropriate ANDROID notification priority for this profile type
             * @see NotificationCompat#PRIORITY_MIN
             * @see NotificationCompat#PRIORITY_DEFAULT
             * @see NotificationCompat#PRIORITY_HIGH
             * @see NotificationCompat#PRIORITY_MAX
             */
            int getPriority();

        }

        String getTitle();

        String getMessage();

        int getCustomIconResId();

        // Return the timestamp in milliseconds
        Long getTimestampInMs();

        PendingIntent getIntent();

        Profile getCustomProfile();

        int getType();

        void activate();
    }

    private class NotificationComparator implements Comparator<NotificationItem> {
        @Override
        public int compare(final NotificationItem item1, final NotificationItem item2) {
            return item1.getTimestampInMs().compareTo(item2.getTimestampInMs());
        }
    }

    /**
     * Required interface to be implemented to have bbm handle component notifications.
     */
    public interface BBMNotificationProvider {

        /**
         * Returns all the notifications currently known.
         */
        Collection<NotificationItem> getNotifications();

        /**
         * Clears the provider the current notifications
         */
        void clear();

        /**
         * Sets a conversation or group uri to ingore. Typically used to
         * set the current conversation/group the user is viewing to be ignored.
         */
        void setIgnoreUri(final String uri);

        /**
         * Each provider is expected to be enabled by default. This does let the manager
         * toggle between enabled/disabled to control if notification handling should
         * occur or not.
         * @param enable True to turn on notification handling. False to turn off.
         */
        void allowNotificationHandling(final boolean enable);
    }
}
