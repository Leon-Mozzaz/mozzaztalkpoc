/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat.util;

import android.content.Context;
import android.text.TextUtils;

import com.bbm.example.richchat.R;
import com.bbm.sdk.common.Equal;

import java.text.NumberFormat;
import java.util.List;
import java.util.regex.Pattern;

public class StringUtil {
    private static final Pattern PATTERN_MULTILINE = Pattern.compile("[\r\t\n]+");

    public static final String EmptyString = "";

    public static final String HTML_FONT_HIGHLIGHT_COLOR = "<font color='#%s'>";
    public static final String HTML_FONT_END = "</font>";

    /**
     * Returns non-null strings verbatim. Converts null strings into empty
     * strings. Never returns null.
     *
     * @param possiblyNull
     * @return
     */
    public static String safeString(final String possiblyNull) {
        return possiblyNull == null ? EmptyString : possiblyNull;
    }

    /**
     * Tests object for null before returning the toString of object. returns EmptyString
     * otherwise.
     */
    public static String safeToString(final Object object) {
        if (object != null) {
            return object.toString();
        }
        return StringUtil.EmptyString;
    }

    /**
     * @return true if the string is null or empty or contains only whitespace
     */
    public static boolean isEmpty(final String string) {
        return Equal.isEqual(safeString(string).trim(), EmptyString);
    }

    /**
     * Removes newline, carriage return and tab characters from a string.
     *
     * @param toBeEscaped string to escape
     * @return the escaped string
     */
    public static String makeSingleLine(final String toBeEscaped) {
        return PATTERN_MULTILINE.matcher(toBeEscaped).replaceAll(" ");
    }

    public static String createSeparatedStringFromList(final List<String> tokens, final String separator) {
        if (tokens == null || separator == null) {
            return null;
        }

        final StringBuilder builder = new StringBuilder();
        for (final String token : tokens) {
            builder.append(token);
            builder.append(separator);
        }
        return builder.toString();
    }

    /**
     * Formats the specified string number using the rules of number format nf.
     *
     * @param number the string number that need to be formatted with nf
     * @param nf the number format
     * @return the formatted string if number is successfully converted to a long. Return number
     * if nf is null, the number is null or the parsing to long failed.
     */
    public static String formatNumber(final String number, final NumberFormat nf) {
        if (number == null || nf == null) {
            return number;
        }

        try {
            return nf.format(Long.parseLong(number));
        }catch (NumberFormatException e) {
            // ignore
        }

        return number;
    }


    public static String trimTrailingSpace(final String src) {
        if (src == null || src.length() == 0)
            return src;

        int end = src.length() - 1;

        while (end >= 0) {
            if(src.charAt(end) > ' ') {
                break;//found non-whitespace
            }
            end--;
        }
        if (end < 0) {
            return "";
        }
        if (end == src.length() - 1) {
            return src;
        }
        return src.substring(0, end + 1);
    }


    public static String createHighlightedHtml(String query, String text, Context context) {
        if (TextUtils.isEmpty(query) || TextUtils.isEmpty(text)) {
            return text;
        }
        int index = text.toLowerCase().indexOf(query.toLowerCase());
        if (index >= 0) {
            StringBuilder builder = new StringBuilder();
            //highlighted text will be the primaryColor of our app
            String colorCodeStr = Integer.toHexString(context.getResources().getColor(R.color.primaryColor) & 0x00ffffff);
            //ensure it's a six-digit hash which represents an html color code
            colorCodeStr = ("000000"+colorCodeStr).substring(colorCodeStr.length());
            builder.append(text.substring(0, index))
                    .append(String.format(HTML_FONT_HIGHLIGHT_COLOR, colorCodeStr))
                    .append(text.substring(index, index + query.length()))
                    .append(HTML_FONT_END)
                    .append(text.substring(index + query.length()));
            return builder.toString();
        }
        return text;
    }
}

