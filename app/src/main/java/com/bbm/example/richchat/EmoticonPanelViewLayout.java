/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;


import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.bbm.example.richchat.util.ViewCoordinateUtils;
import com.bbm.sdk.support.util.Logger;

/**
 * We need to use this as our root layout in views with emoticon panels
 * in order to override dispatchKeyEventPreIme() and handle back button key press.
 *
 *  Use {@link EmoticonInputPanel.LowerPanelVisibilityListener} to listen to lower panel visibility.
 */
public final class EmoticonPanelViewLayout extends LinearLayout {

    private EmoticonPanelControlAble mEmoticonInputPanel;
    private BubbleListTouchListener mBubbleListTouchListener;
    private int mKeyboardHeight;
    private int mViewHeightDeduction;

    private static final int MAX_SCROLL_TO_BE_CONSIDERED_A_CLICK = 30;
    private final Point mPortraitDisplaySize = new Point();
    private final int mNavigationBarSize;
    private final int mStatusBarSize;

    public interface EmoticonPanelControlAble {
        void onKeyBoardShown(int KeyboardHeight);
        void onKeyBoardHidden();
        EmoticonInputPanel.Mode getLowerPanelMode();
    }

    public EmoticonPanelViewLayout(final Context context) {
        this(context, null);
    }

    public EmoticonPanelViewLayout(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EmoticonPanelViewLayout(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        setOrientation(VERTICAL);

        //detect if there is a naviagation bar (ie. GUI based back/home key)
        //and get it's height
        final int resId = getResources().getIdentifier("config_showNavigationBar", "bool", "android");
        if (resId > 0 && getResources().getBoolean(resId)) {
            int bottomBarResourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
            if (bottomBarResourceId > 0) {
                mNavigationBarSize = getResources().getDimensionPixelSize(bottomBarResourceId);
            } else {
                mNavigationBarSize = 0;
            }
        } else {
            mNavigationBarSize = 0;
        }

        //get the height of the top status bar (ie. battery/coverage/notifications)
        int topBarResourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (topBarResourceId > 0) {
            mStatusBarSize = getResources().getDimensionPixelSize(topBarResourceId);
        } else {
            mStatusBarSize = 0;
        }

        //store the display height/width
        final WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        final Display display = wm.getDefaultDisplay();
        display.getSize(mPortraitDisplaySize);
        final int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //swap height&width
            mPortraitDisplaySize.set(mPortraitDisplaySize.y, mPortraitDisplaySize.x);
        }

    }

    public void setEmoticonInputPanel(EmoticonPanelControlAble emoticonInputPanel) {
        mEmoticonInputPanel = emoticonInputPanel;
    }


    /**
     * Override of the removeAllViews, needed as issue with AppCompat-v7:22 lib is causing
     * a crash of the application on close. This is issue is triggered deep in the google code
     * which bubbles up. To be safe we are overriding and trapping the exception to continue
     * to work.
     */
    @Override
    public void removeAllViews() {
        try{
            super.removeAllViews();
        } catch (final Exception e) {
            Logger.e(e, "EmoticonPanelViewLayout - removing views crash");
        }
    }

    /**
     * Override of the removeAllViewsInLayout, needed as issue with AppCompat-v7:22 lib is causing
     * a crash of the application on close. This is issue is triggered deep in the google code
     * which bubbles up. To be safe we are overriding and trapping the exception to continue
     * to work.
     */
    @Override
    public void removeAllViewsInLayout() {
        try{
            super.removeAllViewsInLayout();
        } catch (final Exception e) {
            Logger.e(e, "EmoticonPanelViewLayout - removing all views in layout crash");
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (mEmoticonInputPanel != null) {
            // Since Android does not have callbacks for soft keyboard open/close,
            // this is a workaround to detect whether the soft keyboard is shown/hidden.
            // This is important to keep EmoticonInputPanel in sync of state.
            final int widthSize = MeasureSpec.getSize(widthMeasureSpec);
            final int heightSize = MeasureSpec.getSize(heightMeasureSpec);

            final int orientation = getResources().getConfiguration().orientation;
            //displaysize is stored in portrait configuration
            // get the proper "height" corresponding to the current orientation
            final int screenHeight = orientation == Configuration.ORIENTATION_PORTRAIT ?
                    mPortraitDisplaySize.y : mPortraitDisplaySize.x;
            if(orientation == Configuration.ORIENTATION_LANDSCAPE && widthSize <= screenHeight ||
                    orientation == Configuration.ORIENTATION_PORTRAIT && widthSize >= screenHeight  ) {
                Logger.d("orientation and measurement mismatched, ignore", getClass());
            } else {
                int keyboardHeight = getKeyboardHeight(heightSize, orientation);
                if (mKeyboardHeight != keyboardHeight) {
                    mKeyboardHeight = keyboardHeight;
                    // keyboard visibility changed, update accordingly
                    if (keyboardHeight > 100) {
                        mEmoticonInputPanel.onKeyBoardShown(keyboardHeight);
                    } else if (keyboardHeight >= 0 && orientation == Configuration.ORIENTATION_PORTRAIT
                            || keyboardHeight == 0 && orientation == Configuration.ORIENTATION_LANDSCAPE) {
                        //On screen rotation, VKB is hidden on Landscape orientation
                        mEmoticonInputPanel.onKeyBoardHidden();
                    }
                }

            }
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setOnRootTouchListener(BubbleListTouchListener listener) {
        mBubbleListTouchListener = listener;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if(mBubbleListTouchListener != null) {
            mBubbleListTouchListener.onTouch(this, ev);
        }
        return super.onInterceptTouchEvent(ev);
    }

    public static abstract class BubbleListTouchListener implements View.OnTouchListener {
        private float mDownY;
        private long mDownTimestamp;
        private final View mBubbleView;

        public BubbleListTouchListener(View bubbleView) {
            mBubbleView = bubbleView;
        }

        @Override
        public boolean onTouch(final View view, final MotionEvent ev) {
            if (ev.getAction() == MotionEvent.ACTION_DOWN) {
                onDown();
                mDownY = ev.getY();
                mDownTimestamp = ev.getEventTime();
            } else if (ev.getAction() == MotionEvent.ACTION_UP) {
                onUp();
                float upY = ev.getY();
                long upTime = ev.getEventTime();
                if (upTime - mDownTimestamp < ViewConfiguration.getLongPressTimeout() &&
                        Math.abs(upY - mDownY) < MAX_SCROLL_TO_BE_CONSIDERED_A_CLICK &&
                        ViewCoordinateUtils.coordinateBelongsToView((int)ev.getRawX(), (int)ev.getRawY(), mBubbleView)) {
                    onTap();
                }
            }
            return false;
        }

        public abstract void onDown();
        public abstract void onUp();
        public abstract void onTap();
    }

    /**
     * Computes the keyboard height, so that the lower panel of EmotionInputPanel can be of same
     * height. This makes the transition between keyboard and other modes appear smooth.
     */
    private int getKeyboardHeight(final int rootViewHeight, final int orientation) {
        int rootWidth = getRootView().getWidth();
        int rootHeight = getRootView().getHeight();
        int screenHeight;
        if(orientation == Configuration.ORIENTATION_PORTRAIT) {
            screenHeight = Math.max(rootWidth, rootHeight);
        } else {
            screenHeight = Math.min(rootWidth, rootHeight);
        }

        if (screenHeight <= 0) {
            return 0;
        }

        // minus top status bar, which shows wifi, battery and time
        screenHeight -= mStatusBarSize;

        // minus bottom navigation bar, if it takes part of the screen height(only in portrait mode)
        if( orientation == Configuration.ORIENTATION_PORTRAIT) {
            screenHeight -= mNavigationBarSize;
        }
        return screenHeight - rootViewHeight - mViewHeightDeduction;
    }

    public void setViewHeightDeduction(int viewHeightDeduction) {
        mViewHeightDeduction = viewHeightDeduction;
    }
}
