/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.app.PendingIntent;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.bbm.example.common.ui.messages.MessageHelper;
import com.bbm.sdk.support.util.BbmUtils;
import com.bbm.example.richchat.util.IntentUtils;
import com.bbm.example.richchat.util.RichChatUtils;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.bbmds.Chat;
import com.bbm.sdk.bbmds.ChatMessage;
import com.bbm.sdk.bbmds.User;
import com.bbm.sdk.bbmds.internal.Existence;
import com.bbm.sdk.reactive.SingleshotMonitor;


public class ChatMessageNotificationItem extends SingleshotMonitor implements BBMNotificationManager.NotificationItem {

    private final ChatNotificationModel mChatNotificationModel;

    private ChatMessage mChatMessage;
    private User mUser;
    private String mNotificationTitle;
    private String mNotificationMessage;
    private long mNotificationTimestamp;
    private BBMNotificationManager.NotificationItem.Profile mProfile;
    private Context mContext;

    public ChatMessageNotificationItem(Context context, final ChatNotificationModel chatNotificationModel, final ChatMessage chatMessage) {
        mContext = context;
        mChatNotificationModel = chatNotificationModel;
        mChatMessage = chatMessage;
    }

    @Override
    public String getTitle() {
        return mNotificationTitle;
    }

    @Override
    public String getMessage() {
        return mNotificationMessage;
    }

    @Override
    public int getCustomIconResId() {
        return -1;
    }

    @Override
    public Long getTimestampInMs() {
        return mNotificationTimestamp * 1000;
    }

    @Override
    public PendingIntent getIntent() {
        return IntentUtils.buildPendingIntentForChat(mContext, mChatMessage.chatId);
    }

    @Override
    public Profile getCustomProfile() {
        return mProfile;
    }

    private static class HighPriorityProfile implements Profile {
        @Override
        public String getSoundUri() {
            return null;
        }

        @Override
        public boolean isVibrate() {
            return true;
        }

        @Override
        public int ledColor() {
            return 0;
        }

        @Override
        public int getPriority() {
            return NotificationCompat.PRIORITY_DEFAULT;
        }

    }

    @Override
    public int getType() {
        return TYPE_MESSAGE;
    }

    @Override
    protected boolean runUntilTrue() {
        final Chat chat = BBMEnterprise.getInstance().getBbmdsProtocol().getChat(mChatMessage.chatId).get();

        if (chat.exists != Existence.YES) {
            return false;
        }
        mProfile = new HighPriorityProfile();

        mUser = BBMEnterprise.getInstance().getBbmdsProtocol().getUser(mChatMessage.senderUri).get();
        if (mUser.exists == Existence.MAYBE) {
            return false;
        } else if (mUser.exists == Existence.NO){
            mChatNotificationModel.removeNotification(chat.chatId);
            return true;
        }

        mNotificationTimestamp = mChatMessage.timestamp;

        mNotificationTitle = BbmUtils.getUserName(mUser);
        mNotificationMessage = MessageHelper.getChatMessageFromType(mContext, mChatMessage).toString();

        mChatNotificationModel.addPending(BbmUtils.chatIdToConversationUri(chat.chatId));
        return true;
    }
}
