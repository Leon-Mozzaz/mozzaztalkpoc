/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;

import com.bbm.example.richchat.BBMMainActivity;
import com.bbm.example.richchat.R;
import com.bbm.example.richchat.RichChatActivity;

public class IntentUtils {

    public static PendingIntent buildPendingIntentForChatsTab(final Context context) {
        return buildPendingIntentForTab(context, R.id.slide_menu_item_main_chats);
    }

    private static PendingIntent buildPendingIntentForTab(final Context context, final int type) {
        final Intent mainActivityIntent = new Intent(context, BBMMainActivity.class);
        mainActivityIntent.putExtra(BBMMainActivity.INTENT_EXTRA_ACTIVE_SECTION, type);
        mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        return PendingIntent.getActivity(context, 0, mainActivityIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    public static PendingIntent buildPendingIntentForChat(final Context context, final String chatId) {
        if (chatId != null) {
            final Intent intent = new Intent(context, RichChatActivity.class);
            intent.putExtra(RichChatActivity.EXTRA_CHAT_ID, chatId);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            final Intent mainActivityIntent = new Intent(context, BBMMainActivity.class);
            mainActivityIntent.putExtra(BBMMainActivity.INTENT_EXTRA_ACTIVE_SECTION, R.id.slide_menu_item_main_chats);
            mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            final TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
            taskStackBuilder.addNextIntent(mainActivityIntent);
            taskStackBuilder.addNextIntent(intent);

            return taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_CANCEL_CURRENT);
        }
        return null;
    }
}
