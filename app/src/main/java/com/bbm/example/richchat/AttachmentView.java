/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


public class AttachmentView extends CustomView {

    private final ImageView mThumbnail;
    private final TextView mPrimaryText;
    private final TextView mSecondaryText;
    private final ImageButton mCancelButton;

    public AttachmentView(final Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.view_attachment, this, true);

        mThumbnail = (ImageView) findViewById(R.id.attach_image);
        mThumbnail.setVisibility(View.GONE);

        mPrimaryText = (TextView) findViewById(R.id.attach_primary_text);
        mPrimaryText.setVisibility(View.GONE);

        mSecondaryText = (TextView) findViewById(R.id.attach_secondary_text);
        mSecondaryText.setVisibility(View.GONE);

        mCancelButton = (ImageButton) findViewById(R.id.attach_cancel);

    }

    public void setPrimaryText(final String primaryText) {
        mPrimaryText.setVisibility(View.VISIBLE);
        mPrimaryText.setText(primaryText);
    }

    public void setSecondaryText(final String secondaryText) {
        mSecondaryText.setVisibility(View.VISIBLE);
        mSecondaryText.setText("(" + secondaryText + ")");
    }

    public ImageView getThumbnail() {
        mThumbnail.setVisibility(View.VISIBLE);

        return mThumbnail;
    }

    public void setCancelButtonOnClickListener(final OnClickListener listener) {
        mCancelButton.setOnClickListener(listener);
    }

}
