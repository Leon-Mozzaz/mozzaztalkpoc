package com.bbm.example.richchat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import org.json.JSONObject;
import org.json.JSONException;
import com.google.gson.JsonObject;
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText loginEmail = (EditText) findViewById(R.id.loginEmail);
        final EditText loginPassword = (EditText) findViewById(R.id.loginPassword);
        final Button loginButton = (Button) findViewById(R.id.loginButton11);
        final RequestQueue queue = Volley.newRequestQueue(this);
        loginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                String username = loginEmail.getText().toString().trim();
                String password = loginPassword.getText().toString().trim();
                if (username.length()==0){
                    Toast.makeText(getApplicationContext(), "Please Enter Your User Name.",Toast.LENGTH_LONG).show();
                }
                else if(password.length()==0){
                    Toast.makeText(getApplicationContext(), "Please Enter Your Password. To try the style of this message.",Toast.LENGTH_SHORT).show();
                }

//                Log.i("System.out","email"+loginEmail);
//                Log.i("System.out","password"+loginPassword);
//                String loginRequestURL = "https://server-qa.mozzazcare.com/rpc.ashx";
//                Toast.makeText(getApplicationContext(), "Clicked hello" + loginEmail.getText().toString(),Toast.LENGTH_SHORT).show();
//                JSONObject jsonTest = new JSONObject();
//                final JSONObject requestParamCommon= new JSONObject();
//                try {
//                    requestParamCommon.put("jsonrpc","2.0");
//                    requestParamCommon.put("method","Login");
//                    requestParamCommon.put("id","");
//
//                    JSONObject requestParams = new JSONObject();
//                    requestParams.put("appIdentifier","LT");
//                    requestParams.put("applicationVersion","2.0");
//                    requestParams.put("buildNo","2.3.5");
//                    requestParams.put("apiVersion","1.14");
//                    requestParams.put("requestType","app");
//                    requestParams.put("source","Lifetiles");
//                    requestParams.put("username",loginEmail.getText().toString());
//                    requestParams.put("password",loginPassword.getText().toString());
//                    requestParamCommon.put("params",requestParams);
//
//
//                }catch (JSONException e){
//
//                    e.printStackTrace();
//                }
//                JsonObjectRequest loginRequest = new JsonObjectRequest(Request.Method.POST, loginRequestURL, requestParamCommon, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.v("System.out", "URL request"+requestParamCommon.toString());
//                        Log.v("System.out", "URL request"+response.toString());
//
//                    }}, new Response.ErrorListener(){
//                        @Override
//                        public void onErrorResponse(VolleyError error){
//                            Log.v("System.out", "Volley Message Error"+ error.getMessage());
//                            Log.v("System.out", "Volley ToString"+error.toString());
//
//                        }
//                    }
//                );
//                queue.add(loginRequest);
//                Intent bbmMainIntent = new Intent(MainActivity.this, BBMMainActivity.class);
//                MainActivity.this.startActivity(bbmMainIntent);

            }
        });
    }
}
