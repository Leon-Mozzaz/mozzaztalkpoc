/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.bbm.example.common.ui.messages.MessageHelper;
import com.bbm.example.common.util.DateUtil;
import com.bbm.example.common.util.Utils;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.bbmds.Chat;
import com.bbm.sdk.bbmds.ChatMessage;
import com.bbm.sdk.bbmds.Participant;
import com.bbm.sdk.bbmds.TypingUser;
import com.bbm.sdk.bbmds.internal.Existence;
import com.bbm.sdk.reactive.ComputedList;
import com.bbm.sdk.reactive.ObservableValue;
import com.bbm.sdk.reactive.Observer;
import com.bbm.sdk.support.util.BbmUtils;

public class ChatListItemViewHolder {

    private Context mContext;

    private TextView mTitle;
    private TextView mMessage;
    private TextView mDate;

    //observer to listen for data changes that might need UI changes
    private Observer mObserver;
    private ObservableValue<TypingUser> mTypingUser;

    public ChatListItemViewHolder(final Context context, final View view) {
        mContext = context;
        if (view != null) {
            // Assumption: the view is inflated from list_item_chat
            mTitle = (TextView) view.findViewById(R.id.chat_title);
            mMessage = (TextView) view.findViewById(R.id.chat_message);
            mDate = (TextView) view.findViewById(R.id.chat_date);
        }
    }

    private void setTitle(String title) {
        if (mTitle != null) {
            mTitle.setText(title);
        }
    }

    private void setTitleTypeFace(int typeFace) {
        if (mTitle != null) {
            mTitle.setTypeface(null, typeFace);
        }
    }

    public void setMessage(Spanned message) {
        if (mMessage != null) {
            mMessage.setText(message);
        }
    }

    public void setMessage(String message) {
        if (mMessage != null) {
            mMessage.setText(message);
        }
    }

    private void setDraftMessage(final String message) {
        setMessage(message);
        setMessageWithDrawable(R.drawable.ic_item_message_draft);
        showDate(false);
    }

    private void setTypingMessage() {
        setStatusMessage(getResources().getString(R.string.conversation_is_writing_a_message));
    }

    private void setStatusMessage(final String message) {
        setMessage(message);
        setMessageWithDrawable(R.drawable.ic_item_message_available);
        showDate(false);
    }

    private void setMessageWithDrawable(Drawable drawable) {
        if (mMessage != null) {
            setMessageWithDrawable(mMessage, drawable);
        }
    }

    private void setMessageWithDrawable(int drawableId) {
        if (mMessage != null) {
            setMessageWithDrawable(mMessage, getDrawable(drawableId));
        }
    }

    private void setMessageWithDrawable(final TextView chatMessage, final Drawable lastMessageIcon) {
        MessageHelper.setMessageWithDrawable(getResources(), chatMessage, lastMessageIcon);
    }

    private void setMessageColor(int color) {
        if (mMessage != null) {
            mMessage.setTextColor(getColor(color));
        }
    }

    private void setPriorityTextColor(final ChatMessage lastMessage) {

        if (lastMessage.data != null) {
            String priority = lastMessage.data.optString(MessageHelper.PRIORITY);
            if (MessageHelper.MessagePriorityLevel.HIGH.toString().equals(priority)) {
                setMessageColor(R.color.chat_bubble_alert_text_incoming);
            } else if (MessageHelper.MessagePriorityLevel.LOW.toString().equals(priority)) {
                setMessageColor(R.color.chat_bubble_alert_text_incoming);
            }
        }
    }

    private void setDate(String text) {
        if (mDate != null) {
            mDate.setText(text);
        }
    }

    private void showDate(boolean show) {
        if (mDate != null) {
            mDate.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    public void updateView(final ChatListItem item) {
        setTitleTypeFace(Typeface.NORMAL);
        setMessageColor(R.color.brightstyle_secondaryTextColor);
        //reset values that could be from old one
        mTypingUser = null;

        final ComputedList<Participant> participants = BbmUtils.getConversationParticipantList(item.getUri());
        //get the observable chat just so we can listen for changes on it
        final ObservableValue<Chat> chatObservableValue = BBMEnterprise.getInstance().getBbmdsProtocol().getChat(item.getChat().getPrimaryKey());
        //get the list of participants, wrapped in a observable value and listen for when it changes to update UI
        //this is needed since when the app first loads the data needed to format user names might not be available yet
        //so we need to handle when it is ready (exists) and update UI then otherwise the default (user RegID's) will be displayed
        final ObservableValue<String> formattedParticipants = Utils.getFormattedParticipantList(item.getChat().chatId);

        //create new observer each time, let old one get garbage collected, keep member reference to new one
        mObserver = new Observer() {
            @Override
            public void changed() {
                if (this != mObserver) {
                    participants.removeObserver(this);
                    chatObservableValue.removeObserver(this);
                    formattedParticipants.removeObserver(this);
                    return;
                }

                Chat chat = chatObservableValue.get();

                final boolean emptyChat = chat.numMessages == 0 || chat.lastMessage == 0;

                // Chat Title
                String title;
                if (!TextUtils.isEmpty(chat.subject)) {
                    title = chat.subject;
                } else {
                    if (participants.size() == 0) {
                        title = mContext.getResources().getString(R.string.chats_empty_chat);
                    } else {
                        title = formattedParticipants.get();
                    }
                }
                setTitle(title);

                boolean otherUserTyping = false;
                if (!item.isConference()) {
                    if (mTypingUser == null) {
                        if (participants.size() > 0) {
                            Participant firstParticipant = participants.get(0);
                            if (firstParticipant.getExists() == Existence.YES && !TextUtils.isEmpty(firstParticipant.userUri)
                                    && !TextUtils.isEmpty(item.getUri())) {

                                final TypingUser.TypingUserKey lookupKey = new TypingUser.TypingUserKey(firstParticipant.userUri, item.getUri());
                                mTypingUser = BBMEnterprise.getInstance().getBbmdsProtocol().getTypingUser(lookupKey);
                                //need to listen to it for changes
                                mTypingUser.addObserver(this);
                            }
                        }
                    }

                    if (mTypingUser != null && mTypingUser.get().exists == Existence.YES) {
                        otherUserTyping = true;
                    }
                }

                // Chat message
                final String draftMessage = BbmUtils.getDraftMessage(chat);
                final boolean hasDraft = !TextUtils.isEmpty(draftMessage);
                if (hasDraft) {
                    setDraftMessage(draftMessage);
                } else if (otherUserTyping) {
                    setTypingMessage();
                } else if (emptyChat) {
                    setStatusMessage("");
                } else {
                    final ChatMessage.ChatMessageKey lookupKey = new ChatMessage.ChatMessageKey(chat.chatId, chat.lastMessage);
                    ObservableValue<ChatMessage> lastChatMessageObservable = BBMEnterprise.getInstance().getBbmdsProtocol().getChatMessage(lookupKey);
                    //need to listen to this message if its read or other state that affects the UI changes
                    lastChatMessageObservable.addObserver(this);
                    ChatMessage lastChatMessage = lastChatMessageObservable.get();

                    if (lastChatMessage.getExists() == Existence.YES) {
                        setMessage(MessageHelper.getChatMessageFromType(mContext, lastChatMessage));

                        // Check if last Message is a non-expired and is High Priority
                        setPriorityTextColor(lastChatMessage);

                        // Process last message status
                        // Set last message icon
                        setMessageWithDrawable(getChatMessageStateIcon(lastChatMessage, item.isConference()));

                        if (lastChatMessage.hasFlag(ChatMessage.Flags.Incoming)) {
                            if (lastChatMessage.state != ChatMessage.State.Read) {
                                setTitleTypeFace(Typeface.BOLD);
                            } else {
                                setTitleTypeFace(Typeface.NORMAL);
                            }
                        }

                        // Chat Date
                        if (lastChatMessage.timestamp > 0) {
                            setDate(DateUtil.observableChatBubbleHeaderTimestamp(mContext, lastChatMessage.timestamp));
                        } else {
                            setDate("");
                        }
                        showDate(true);
                    }
                }
            }
        };

        //listen for changes to anything we use to display to UI
        participants.addObserver(mObserver);
        chatObservableValue.addObserver(mObserver);
        formattedParticipants.addObserver(mObserver);

        //call it right away to update UI with current state
        mObserver.changed();
    }

    public Resources getResources() {
        return mContext.getResources();
    }

    private Drawable getDrawable(final int resourceId) {
        return getResources().getDrawable(resourceId);
    }

    private int getColor(final int resourceId) {
        return getResources().getColor(resourceId);
    }

    private Drawable getChatMessageStateIcon(ChatMessage lastChatMessage, boolean isConference) {
        if (lastChatMessage.hasFlag(ChatMessage.Flags.Incoming)) {
            return MessageHelper.getIncomingChatStateDrawable(getResources(), lastChatMessage);
        } else {
            return MessageHelper.getOutgoingChatStateDrawable(getResources(), lastChatMessage, isConference);
        }
    }
}
