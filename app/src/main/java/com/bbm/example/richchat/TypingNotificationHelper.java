/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;


import android.os.Handler;

import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.bbmds.outbound.TypingNotification;

public class TypingNotificationHelper {

    private static final int NOTIFY_USER_TYPING_DELAY = 5000; // 5 seconds

    private final Handler mHandler = new Handler();

    private boolean mIsStarted = false;
    private final String mConvUri;
    private long mLastTimeTypingNotificationSent = -1;


    public TypingNotificationHelper(final String convUri) {
        mConvUri = convUri;
    }

    private final Runnable mNotifyUserTyping = new Runnable() {

        @Override
        public void run() {
            mHandler.removeCallbacks(mNotifyUserTyping);

            BBMEnterprise.getInstance().getBbmdsProtocol().send(new TypingNotification(mConvUri, true));
            mLastTimeTypingNotificationSent = System.currentTimeMillis();
            mIsStarted = false;
        }
    };

    public void notifyUserTyping() {
        if (!mIsStarted) {
            mIsStarted = true;
            mHandler.postDelayed(mNotifyUserTyping, NOTIFY_USER_TYPING_DELAY);
        }
    }


    public void resetHelper() {
        mHandler.removeCallbacks(mNotifyUserTyping);
        mIsStarted = false;
        mLastTimeTypingNotificationSent = -1;
    }

    public void stopNotifier() {
        if (mIsStarted) {
            mHandler.removeCallbacks(mNotifyUserTyping);
        }
    }

    public long getLastTimeTypingNotification() {
        return mLastTimeTypingNotificationSent;
    }

}
