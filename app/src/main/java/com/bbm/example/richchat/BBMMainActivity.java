/* Copyright (c) 2017 BlackBerry.  All Rights Reserved.
*
* You must obtain a license from and pay any applicable license fees to
* BlackBerry before you may reproduce, modify or distribute this
* software, or any work that includes all or part of this software.
*
* This file may contain contributions from others. Please review this entire
* file for other proprietary rights or license notices.
*/

package com.bbm.example.richchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.bbm.example.common.reactive.BbmDeviceSwitchHelper;
import com.bbm.example.richchat.fragments.ContactsFragment;
import com.bbm.example.richchat.fragments.GoogleConnectSetupFragment;
import com.bbm.example.richchat.util.RichChatUtils;
import com.bbm.sdk.BBMEnterprise;
import com.bbm.sdk.bbmds.outbound.RetryServerRequests;
import com.bbm.sdk.support.identity.auth.google.auth.GoogleAuthHelper;
import com.bbm.sdk.support.identity.user.AppUser;
import com.bbm.sdk.support.identity.user.firebase.FirebaseUserDbSync;
import com.bbm.sdk.support.protect.ProtectedManager;
import com.bbm.sdk.support.util.Logger;

import java.util.List;


public class BBMMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ContactsFragment.OnListFragmentInteractionListener {

    public static final String INTENT_EXTRA_ACTIVE_SECTION = "active_section";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //listen for messages from BBM SDK when the users BBM account is used in another device or another app on this device
        //most apps would need to customize this or write their own handling
        //Log.i("System.out","hello Mozzaz Talk");
        //setContentView(R.layout.activity_login);
        BbmDeviceSwitchHelper.getInstance().listenForAndHandleDeviceSwitch(this);
        BbmDeviceSwitchHelper.getInstance().listenForAndHandleGoAway(this);

        //prompt the user to sign in with their Google account, and pass that data to our user manager when ready
        GoogleAuthHelper.initGoogleSignIn(this, FirebaseUserDbSync.getInstance(), getString(R.string.default_web_client_id));

        setContentView(R.layout.activity_bbmmain);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //don't reload default fragment on rotate...
        if (savedInstanceState == null) {
            final FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragmentContent, new ChatsFragment()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    /**
     * The google sign in will send the results to this activity, this just passes to the helper to pass auth info to BBM SDK
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GoogleAuthHelper.RC_GOOGLE_SIGN_IN_ACTIVITY) {
            GoogleAuthHelper.handleOnActivityResult(this, FirebaseUserDbSync.getInstance(), requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Logger.d("onNavigationItemSelected: "+item);
        int id = item.getItemId();

        Fragment fragment = null;
        if (id == R.id.nav_status) {
            fragment = new GoogleConnectSetupFragment();
        } else if (id == R.id.nav_chats) {
            fragment = new ChatsFragment();
        } else if (id == R.id.nav_contacts) {
            fragment = new ContactsFragment();
        }

        if (fragment != null) {
            final FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragmentContent, fragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        Logger.d("onNewIntent: intent="+intent);
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        Logger.d("onResume: getIntent()="+intent);

        if (intent.getIntExtra(INTENT_EXTRA_ACTIVE_SECTION, 0) == R.id.slide_menu_item_main_chats) {
            //prevent this from happening again, otherwise if user selects another fragment, then leaves app and comes back it will switch back again
            intent.removeExtra(INTENT_EXTRA_ACTIVE_SECTION);

            final FragmentManager fragmentManager = getSupportFragmentManager();

            List<Fragment> fragmentList = fragmentManager.getFragments();
            boolean showChatsFragment = true;

            if (fragmentList != null) {
                for (Fragment fragment : fragmentList) {
                    if (fragment instanceof ChatsFragment) {
                        showChatsFragment = false;
                        if (fragment.isVisible()) {
                            Logger.d("onResume: already showing ChatsFragment");
                        } else {
                            Logger.d("onResume: showing ChatsFragment="+fragment);
                            fragmentManager.beginTransaction().replace(R.id.fragmentContent, fragment).commit();
                        }
                    }
                }
            }

            if (showChatsFragment) {
                Logger.d("onResume: loading ChatsFragment");
                fragmentManager.beginTransaction().replace(R.id.fragmentContent, new ChatsFragment()).commit();
            }
        }

        //Allow BBM to retry any failed server messages
        BBMEnterprise.getInstance().getBbmdsProtocol().send(new RetryServerRequests());
        ProtectedManager.getInstance().retryFailedEvents();
    }

    @Override
    public void onListFragmentInteraction(AppUser user) {
        Logger.d("onListFragmentInteraction: "+user);

        if (user != null && user.getRegId() != 0) {
            RichChatUtils.createNewChat(this, new long[] {user.getRegId()}, null);
        }
    }
}
