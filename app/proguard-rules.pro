-keepattributes EnclosingMethod
-keepattributes InnerClasses

#Duplicate library definition notes:
-dontnote android.net.http.*
-dontnote org.apache.commons.codec.**
-dontnote org.apache.http.**
-dontnote com.google.android.gms.**
-dontnote com.google.common.cache.**
-dontnote com.google.appengine.api.**
-dontnote com.google.apphosting.api.**
-dontnote com.google.firebase.**

#Rich chat:
-keep,includedescriptorclasses public class com.bbm.example.richchat.EmoticonInputPanel$OnActionClickedListener{ *; }
-keep,includedescriptorclasses public class com.bbm.example.richchat.EmoticonInputPanel$Mode{ *; }
-keep,includedescriptorclasses public class com.bbm.example.richchat.EmoticonPanelViewLayout$EmoticonPanelControlAble{ *; }
-keep,includedescriptorclasses public class com.bbm.example.richchat.EmoticonPanelViewLayout$BubbleListTouchListener{ *; }